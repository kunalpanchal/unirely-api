"use strict";

const mongoose = require('mongoose');
const UniversityUsersSchema = require('./schemas/university-mentors');

module.exports = mongoose.model('UniversityUsers', UniversityUsersSchema);