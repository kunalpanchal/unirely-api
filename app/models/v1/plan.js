"use strict";

const mongoose = require('mongoose');
const PlanSchema = require('./schemas/plan');

module.exports = mongoose.model('Plan', PlanSchema);