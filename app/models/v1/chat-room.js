"use strict";

const mongoose = require('mongoose');
const ChatRoomSchema = require('./schemas/chat-room');

module.exports = mongoose.model('ChatRoom', ChatRoomSchema);