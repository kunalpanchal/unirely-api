'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FeatureSchema = new Schema(
    {
        _id:{
            type: String,
            required: true,
            unique: true,
            trim: true
        },

        name:{
            type: String,
            required: true,
            trim: true,
            minlength: 3,
        },

        description:{
            type: String,
            default: null
        },

        active:{
            type: Boolean,
            default: true 
        },

        metas:[
            {
                key: {
                    type: String,
                    default: null
                },
                value: {
                    type: String,
                    default: null
                }
            }
        ],
        
        parent:{
            type: String,
			ref: 'Feature',
			default: null
        }
    },
    {
        timestamps: true,
        versionKey: false,
        autoIndex: true
    }

);

module.exports = FeatureSchema;