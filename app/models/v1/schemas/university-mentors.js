"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UniversityUsersSchema = new Schema (
	{
		university: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'University'
		},

		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},

		type: {
			type: String,
			enum: [ 'MENTOR', 'counselor', 'STUDENT' ]
		}
	},
	{
		timestamp: true,
		autoIndex: true,
		versionKey: false
	}
);

module.exports = UniversityUsersSchema;