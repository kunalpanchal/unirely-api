'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlanFeatureSchema = new Schema(
    {
        plan:{
			type: mongoose.Schema.Types.ObjectId,
            ref: 'Plan',
        },

        description:{
            type: String,
            default: null
        },

        active:{
            type: Boolean,
            default: false 
        },

        metas:[
            {
                key: {
                    type: String,
                    default: null
                },
                value: {
                    type: String,
                    default: null
                }
            }
        ],
        
        features:[{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Feature',
		}],

		deleted:{
			type: Boolean,
			default: false
		}

    },
    {
        timestamps: true,
        versionKey: false,
        autoIndex: true
    }

);

module.exports = PlanFeatureSchema;