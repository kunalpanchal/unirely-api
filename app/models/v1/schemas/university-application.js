"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UniversityApplicationSchema = new Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},

		university: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'University'
		},

		mentor: {
			person: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
				default: null
			},
			status: {
				type: String,
				enum: ['PENDING', 'ACCEPTED', 'REJECTED'],
				default: 'PENDING'
			}
		},

		counselor: {
			person: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'User',
				default: null
			},
			status: {
				type: String,
				enum: ['PENDING', 'ACCEPTED', 'REJECTED'],
				default: 'PENDING'
			}
		},

		admin: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},

		approved: {
			type: Boolean,
			default: false
		},

		status:{
			type: String,
			enum: ['IN_ACTIVE','ACTIVE'],
			default: 'ACTIVE'
		},

		chat_room: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'ChatRoom',
			default: []
		}],

		addon:{
			type: Object,
			default: null
		}
	},
	{
		timestamps: true,
		versionKey: false,
		autoIndex: true
	}
);

module.exports = UniversityApplicationSchema;