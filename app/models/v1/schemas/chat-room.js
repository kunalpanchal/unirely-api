"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatRoomSchema = new Schema(
    {
        dialog_id: {
            type: String,
            required: true
        },

        group_id:{
            type: String,
            default: null
        },

        users: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
		}],

		room_name:{
			type: String,
			default: null
        },
        
        status:{
            type: String,
            enum: ['CONNECT', 'DISCONNECT'],
            default: 'CONNECT'
        }
    },
    {
        autoIndex: true,
        versionKey: false
    }
);

module.exports = ChatRoomSchema;