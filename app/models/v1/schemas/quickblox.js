'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuickBloxSchema = new Schema(
    {
        "id": {
            type: String,
            default: null
        },

        "login": {
            type: String,
            default: null
        },

        "email": {
            type: String,
            default: null
        },

        "password": {
            type: String,
            default: null
        },

        "full_name": {
            type: String,
            default: null
        }
    },
    {
        timestamps: true,
        versionKey: false
    }
);

module.exports = QuickBloxSchema;