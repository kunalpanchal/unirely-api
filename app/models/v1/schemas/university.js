'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UniversitySchema = new Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
            unique: true
        },
        image: {
            type: String,
            default: null
        },
        description: {
            type: String,
            trim: true,
            default: null,
        },
        address: {
            type: String,
            trim: true,
            default: null
        },
        contact: {
            type: String,
            trim: true,
            default: null,
		},
		addon:{
			type: String,
			default:null
        },
        price:{
            type: Number,
            default: 0
        },
        priority:{
            type: Boolean,
            default: false
        },
        country:{
            type: String,
            default: null
        }
    },
    {
        autoIndex: true,
        timestamps: true,
        versionKey: false
    }
);

module.exports = UniversitySchema;