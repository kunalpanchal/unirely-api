"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const QuickBloxSchema = require('./quickblox');
const StudentDetailsSchema = require('./student-details');

const _hash = require('./../../../helpers/v1/hash');

const UserSchema = new Schema(
    {
        'email': {
            type: String,
            required: true,
            unique: true,
            trim: true
        },

        'password': {
            type: String,
            required: true,
			set: _hashPassword
        },

        'roles': [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Role'
        }],

        'verified': {
            type: String,
            enum: ['APPROVED', 'REJECTED', 'PENDING'],
            default: 'PENDING'
        },

		'verification_hash': {
        	type: String,
			default: null
        },
        
        'quickblox': QuickBloxSchema,

	    'student_details': StudentDetailsSchema,

        'metas': [
            {
                key: {
                    type: String,
                    default: null
                },
                value: {
                    type: String,
                    default: null
                }
            }
        ],

        'authenticate': [
            {
                type:{
                    type: String,
                    enum: ['facebook', 'google'],
                    default: null,
                },

                id:{
                    type: String,
                    default: null
                }
            }
        ]
    },
    {
        timestamps: true,
        autoIndex: true,
        versionKey: false
    }
);

UserSchema.virtual('full_name')
    .set(function (full_name) {
        let name = full_name.split(' ');
        let data = [
            { key: 'first_name', value: name[0] || full_name },
            { key: 'last_name', value: name[1] || null }
        ];
        this.metas.push(...data);
    })
    .get(function () {
        let names = this.metas
            .filter(meta => meta.key === 'first_name' || meta.key === 'last_name')
            .map(meta => meta.value);

        return `${names.join(' ')}`;
    })

UserSchema.virtual('meta_data')
    .set(function(metas){
        console.log('metaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
        let first_name = metas.find(meta=>meta.key == 'first_name');
        let last_name = metas.find(meta=> meta.key == 'last_name');
        this.student_details['full_name'] = last_name ? first_name+ ' ' +last_name: first_name;
        this.metas.push(...meta_data);
    })
function _hashPassword (password) {
	return _hash.generateHashSyncFor(password);
}

module.exports = UserSchema;