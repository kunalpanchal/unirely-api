'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentDetailsSchema = new Schema (
	{
		user: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		},
		full_name: {
			type: String,
			default: ''
		},
		contact: {
			type: String,
			default: ''
		},
		country: {
			type: String,
			default: ''
		},
		current_high_school: {
			type: String,
			default: ''
		},
		current_class: {
			type: String,
			default: ''
		},
		average_percentage: {
			type: String,
			default: ''
		},
		target_course: {
			type: String,
			default: ''
		},
		sat1_score: {
			type: String,
			default: ''
		},
		sat2_score: {
			type: String,
			default: ''
		},
		advanced_placement_score: {
			type: String,
			default: ''
		},
		toefl_score: {
			type: String,
			default: ''
		},
		ielts_score: {
			type: String,
			default: ''
		},
		target_university_categories: {
			type: Array,
			default: []
		},
		resume_link: {
			type: String,
			default: ''
		},
		source: {
			type: Array,
			default: []
		},
		university_generate_form:{
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true,
		versionKey: false,
		autoIndex: true
	}
);

module.exports = StudentDetailsSchema;