'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlanSchema = new Schema(
    {
        _id:{
            type: String,
            required: true,
            unique: true,
            trim: true
        },

        name:{
            type: String,
            required: true,
            trim: true,
            minlength: 3,
        },

        description:{
            type: String,
            default: null
        },

        price:{
            type: Number,
            default: 0
        },

		meta_data:{
			type: Object,
			default: null
		},

        active:{
            type: Boolean,
            default: false 
		},

		university_limit:{
			type: Number,
			default: -1
		}
    },
    {
        timestamps: true,
        versionKey: false,
        autoIndex: true
    }

);

module.exports = PlanSchema;