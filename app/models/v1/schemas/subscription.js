"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubscriptionSchema = new Schema(
    {
        chargebee_customer_id: {
            type: String,
			required: true,
			unique: true
        },

        chargebee_subscription_id:{
            type: String,
			default: null,
			unique: true
        },

        chargebee_plan_id:{
            type: String,
            default: null
        },

        configured:{
            type: Boolean,
            default: false
        },

        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
		},

		current_usage:{
            university: {
                type: Number,
                default: 0
            },
        },
        
        current_limit:{
            university: {
                type: Number,
                default: -1
            },
        },
        
        invoice:[
            {
                mode:{
                    type: String,
                    enum: ["CASH", "CARD", "OTHER"],
                    default: null
                },
                id:{
                    type: String,
                    default: null
                },
                university_application:[{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'UniversityApplication',
                    default: null
                }],
                plan:{
                    type: String,
                    default: null
                }
            }
        ]
    },
    {
        timestamps: true,
        autoIndex: true,
        versionKey: false
    }
);

module.exports = SubscriptionSchema;