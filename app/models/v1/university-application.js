"use strict";

const mongoose = require('mongoose');
const UniversityApplicationSchema = require('./schemas/university-application');

module.exports = mongoose.model('UniversityApplication', UniversityApplicationSchema);