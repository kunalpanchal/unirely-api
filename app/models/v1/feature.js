"use strict";

const mongoose = require('mongoose');
const FeatureSchema = require('./schemas/feature');

module.exports = mongoose.model('Feature', FeatureSchema);