"use strict";

const mailgun = require('mailgun-js');
const env = require('../../../config/environments/index');
const BaseMailer = require('../../interfaces/base-mailer');

class MailGun extends BaseMailer {

    constructor (config) {
        super(config);
    }

    initialize (config) {
        this.mailer = mailgun({ apiKey: config.apiKey, domain: config.domain });
    }

    sendMail (data) {
        return this.mailer.messages().send(data);
    }
}

module.exports = new MailGun({ apiKey: env.MAILGUN_API_KEY, domain: env.MAILGUN_DOMAIN });