"use strict";

const QB = require('quickblox').QuickBlox;
const env = require('../../../config/environments');
const BaseChatService = require('../../interfaces/base-chat');
const request = require('request')
class QuickBlox extends BaseChatService {
    constructor(config) {
        super(config);
    }

    initialize(config) {
        return new Promise((resolve, reject) => {
            this.QB = new QB();
            this.QB.init(config.appId, config.authKey, config.authSecret);
        });
    }

    createSession () {
        return new Promise((resolve, reject) => {
            this.QB.createSession(function (err, session) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(session);
                }
            });
        });
    }

    createUserSession(login, password){
        return new Promise((resolve, reject) => {
            this.QB.createSession({login: login, password: password},function (err, session) {
                if (err) {
                    console.log(err)
                    reject(err);
                }
                else {
                    resolve(session);
                }
            });
        });
    }

    createUser (data) {
        return new Promise ((resolve, reject) => {
            this.QB.users.create(data, function (err, user) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(user);
                }
            });
        });
    }

    createDialog(data, type=null){
        return new Promise((resolve, reject) => {
            let params = {
                type: type == 'chat' ? 3 : 2,
                occupants_ids: data.opponent_ids,
                photo: 'https://cdn.filestackcontent.com/vddlfPS7SROQ5P72782R',
                name: data.name
            };

            this.QB.chat.dialog.create(params, function (err, createdDialog) {
                if (err) {
                    reject(err);
                } else {
                    resolve(createdDialog);
                }
            });
        })
    }

    getUserByEmail (email) {
        return new Promise((resolve, reject) => {
            this.QB.users.get({ email }, function (err, user) {
                if (user) {
                    resolve(user);
                }
                else {
                    reject(err);
                }
            });
        });
    }

    deleteUserById (id) {
        console.log('Got a new delete', id);
        return new Promise((resolve, reject) => {
            this.QB.users.delete({ external: id }, function (err, user) {
                if (user) {
                    console.log('If', user);
                    resolve(user);
                }
                else {
                    console.log('error', err);
                    reject();
                }
            });
        });
    }

    deleteUserByEmail (email) {
        return new Promise((resolve, reject) => {
            this.QB.users.delete({ email }, function (err, user) {
                if (user) {
                    console.log('If', user);
                    resolve(user);
                }
                else {
                    console.log('error', err);
                    reject();
                }
            });
        });
    }

    connectChat(userId, password, data) {
        return new Promise((resolve, reject) => {
            let self = this;

            console.log(userId, password)
           this.QB.chat.connect({ userId: userId, password: password }, function (err, roster) {
                if (err) {
                    console.log(err, 'Connect');
                    reject(err);
                } else {
                    //self.chatMessage(data.id, data.text, data.type, self)
                    console.log(roster)
                    resolve(roster);
                }
            })
        })
    }

    chatMessage(opponentId, text, type) {
            let msg = {
                type: type,
                body: text,
                extension: {
                    save_to_history: 1,
                    // name: name,
                    // attachments: [attachment]
                },
            };
            if(type == 'chat') opponentId = Number(opponentId);

            console.log(msg, opponentId, type)
            this.QB.chat.send(Number(opponentId), msg);
    }

    disconnectChat(){
        this.QB.chat.disconnect();
    }
}


module.exports = new QuickBlox({ appId: env.QB_APP_ID, authKey: env.QB_AUTH_KEY, authSecret: env.QB_AUTH_SECRET });