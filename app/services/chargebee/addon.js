'use strict';

const chargebee = require('chargebee');

class ChargebeeAddon {
     
    constructor(){}

    async create(data, type=null){
        try{
			let addon_id = data.name.split(' ').join('_').toLowerCase();
			let addon = await this.fetch(addon_id);
			console.log(addon)
			if(!addon.addon){
            	addon = await chargebee.addon.create({
					id : addon_id, 
					name : data.name, 
					invoice_name : "Basic Plan University Addon", 
					charge_type : "non_recurring",
					price : data.price*100, 
					type : "on_off"
				}).request();
			} else if(type !== 'seed'){
                addon =  await chargebee.addon.update(addon_id, {
                    price : data.price*100
                }).request();
            }
            return addon;
        }
        catch(error){
            throw error;
        }
	}
	
	async fetch(addonId){
        try{
            let addon = await chargebee.addon.retrieve(addonId).request();
            return addon;
        }
        catch(error){
            return error;
        }
    }
}

module.exports = new ChargebeeAddon();