'use strict';

const chargebee = require('chargebee');

class ChargebeeCustomer {

    constructor() { }

    async create(user) {
        try{
            let customer_list = await chargebee.customer.list({
                limit: 1,
                'email[is]': user.email
            }).request();
            let customer = null;
        // console.log(user._id.toString().replace(/\d+/g, ''))
            if (customer_list.list.length == 0) {
                let customerResult = await chargebee.customer.create({
                    id: user.email + '_unirely',
                    first_name: user.quickblox.full_name.split(' ')[0],
                    last_name: user.quickblox.full_name.split(' ')[1],
                    email: user.email,
                    meta_data: {
                        'user_id': user._id
                    }
                }).request();
                customer = customerResult.customer;
            } else {
                customer = customer_list.list[0].customer;
            }
            return customer;
        }
        catch(error){
            return error;
        }
    }

    async update(customer_id, data=null){
        try{
            let options = {auto_collection: data.auto_collection};
            let customer = await chargebee.customer.update(customer_id, options).request();
            return customer;
        }
        catch(error){
            return error;
        }
    }
}

module.exports = new ChargebeeCustomer();