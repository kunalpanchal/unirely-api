'use strict';

const chargebee = require('chargebee');

class ChargebeePlan {
     
    constructor(){}

    async create(data){
        try{
            const plan_body = {
                id: data._id,
                name: data.name,
				price: data.price*100,
				meta_data: data.meta_data || null,
				currency_code: "USD"
            }
            let plan = await chargebee.plan.create(plan_body).request();
            return plan;
        }
        catch(error){
            throw error
        }
	}
	
	async fetch(plan_id){
        try{
			let plan = await chargebee.plan.retrieve(plan_id).request();
            return plan;
        }
        catch(error){
            throw error
        }
	}
	
	async update(plan_id, plan_body){
        try{
			let plan = await chargebee.plan.update(plan_id, plan_body).request();
            return plan;
        }
        catch(error){
            throw error
        }
	}
	
	async delete(plan_id){
        try{
			let plan = await chargebee.plan.delete(plan_id).request();
            return plan;
        }
        catch(error){
            throw error
        }
    }
}

module.exports = new ChargebeePlan(); 