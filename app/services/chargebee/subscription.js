'use strict';

const chargebee = require('chargebee');

class ChargebeeSubscription {
     
    constructor(){}

    async create(customer_id, plan){
        try{
            let subscription = await chargebee.subscription.create_for_customer(customer_id, {plan_id: plan}).request();
            return subscription;
        }
        catch(error){
            return error; 
        }
	}
	
	async update(subscription_id, addons=null){
        try{
            let options = {addons: addons};
            let subscription = await chargebee.subscription.update(subscription_id, options).request();
            return subscription;
        }
        catch(error){
            return error;
        }
    }

    async extraCharge(subscription_id, amount, description){
        try{
            let invoice = await chargebee.invoice.charge({
                subscription_id : subscription_id, 
                amount : amount*100, 
                description : description
              }).request();
            return invoice;
        }catch(error){
            return error;
        }
    }
}

module.exports = new ChargebeeSubscription();