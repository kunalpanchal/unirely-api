'use strict';

const chargebee = require('chargebee');

class ChargebeeCard {
     
    constructor(){}

    async create(customer_id, card){
        try{
			console.log(customer_id,'...............................');
            let card_detail = await this.fetch(customer_id);
            if(!card_detail.card){
                card_detail = await chargebee.payment_source.create_card(
                                        {
                                            customer_id:customer_id, 
                                            card : card
                                        }).request();
			}
			else{
				card_detail = await chargebee.card.update_card_for_customer(customer_id,{
										gateway: 'chargebee', 
										card
								})
			}
			console.log(card_detail);
            return card_detail;
        }
        catch(error){
            throw error;
        }
    }

    async fetch(customer_id){
        try{
            
			let card_detail = await chargebee.card.retrieve(customer_id).request();
            return card_detail;
        }
        catch(error){
			return error;
        }
    }
}

module.exports = new ChargebeeCard();