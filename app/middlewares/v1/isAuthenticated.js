"use strict";

const passport = require('passport');
const _err = require('../../helpers/v1/error');
const response = require('../response');

module.exports = (role = '') => {
    return (req, res, next) => {
        try {
            switch (role) {

                case '':
                    passport.authenticate('anyBearer', { session: false })(req, res, next);
                    break;

                case 'admin':
                    passport.authenticate('adminBearer', { session: false })(req, res, next);
                    break;

                case 'mentor':
                    passport.authenticate('mentorBearer', { session: false })(req, res, next);
                    break;

                case 'student':
                    passport.authenticate('studentBearer', { session: false })(req, res, next);
                    break;

                case 'counselor':
                    passport.authenticate('counselorBearer', { session: false })(req, res, next);
                    break;
                case 'counselor-or-admin':
                    passport.authenticate('counselorAdminBearer', { session: false })(req, res, next);
                    break;
                case 'mentor-or-admin':
                    passport.authenticate('mentorAdminBearer', { session: false })(req, res, next);
                    break;

                case 'mentor-or-counselor':
                    passport.authenticate('mentorcounselorBearer', { session: false })(req, res, next);
                    break;

                case 'student-or-admin':
                    passport.authenticate('studentAdminBearer', { session: false })(req, res, next);
                    break;

                case 'client':
                    passport.authenticate('clientBasic', { session: false })(req, res, next);
                    break;

                case 'basic':
                    passport.authenticate('basic', { session: false })(req, res, next);
                    break;

                default:
                    throw _err.createError('UNSUPPORTED_STRATEGY');
            }
        }

        catch (err) {
            response.error(res, err);
        }
    }
};