"use strict";
const envConfig = require('./../../../config/environments');

const _hash = require('./../../helpers/v1/hash');
const _err = require('./../../helpers/v1/error');

const __mailgun = require('./../../services/v1/mailgun');
const __QB = require('./../../services/v1/quickblox');

const mailTemplate = require('../../../config/mailer/templates');

module.exports = {

    sendAccountVerificationEmail (hash, to, from, role) {
    	try {
		    const link = `${envConfig.APP_URL}/oauth/verify/?email=${to}&@&hash=${hash}`;
		    const accountVeificationEmailData = {
			    to,
			    from,
			    subject: 'Verify your account',
			    html: mailTemplate.get('account-verification', { link })
            };
            const adminNotificationEmailData = {
			    to: envConfig.MAILGUN_MAIL,
			    from: to,
			    subject: 'UniRely New SignUp Notify',
			    html: mailTemplate.get('singup-notification-admin', { user: to, role })
            };

            console.log(accountVeificationEmailData,'*******Mail Send********', adminNotificationEmailData);
		    return {
                account_verification: __mailgun.sendMail(accountVeificationEmailData),
                admin_notification:  __mailgun.sendMail(adminNotificationEmailData)
            }
	    }
	    catch(err) { console.log(err); }
    },

    sendPasswordResetMail (hash, to, from, receiver) {
        const link = `${envConfig.APP_URL}/reset-password/?email=${to}&@&hash=${hash}`;
        const emaildata = {
            to,
            from,
            subject: 'Reset your password',
            html: mailTemplate.get('reset-password', { link, receiver })
        };
        return __mailgun.sendMail(emaildata);
    },

    async createQuickBloxUser (qbData) {
        try {
            await __QB.createSession();
            const qbUser = await __QB.createUser(qbData);
            return {
                id: qbUser.id,
                full_name: qbUser.full_name,
                email: qbUser.email,
                login: qbUser.login,
                password: qbData.password
            };
        }
        catch (err) {
            console.log(err);
            if (err.detail.email || err.detail.login) {
	            return this.fetchQuickBloxUser(qbData.email, qbData.password);
            }
            else
                throw _err.createError('QUICKBLOX_ERROR', err.message );
        }
    },

    async createQuickbloxDialog (data, admin){
        try{
            await __QB.createUserSession(admin.login, admin.password);
            let dialog_data = {
                opponent_ids: [data.id],
                name: `${admin.id}-${data.id}`
            };
            const qbDialog = await __QB.createDialog(dialog_data, 'chat');
            return qbDialog;
        }
        catch(err){
            console.log(err);
            throw _err.createError('QUICKBLOX_ERROR', err.message );
        }
    },

    async fetchQuickBloxUser(email, pwd) {
        try {
            await __QB.createSession();
	        const qbUser = await __QB.getUserByEmail(email);
	        return {
		        id: qbUser.id,
		        full_name: qbUser.full_name,
		        email: qbUser.email,
		        login: qbUser.login,
		        password: pwd
	        };
        }
        catch(err) {
            return null;
        }
    }

}