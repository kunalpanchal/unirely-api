"use strict";

const bcrypt = require('bcrypt');
const env = require('../../../config/environments');
const SECRET = process.env.SECRET || 'ghO/I-uYjYTM[>n7hQ;a|nJlasex1&`/*-ut[uQ-wR33G#Dk$X}Me&g3tg~0_*.7WIK~M'
const fetch = require('node-fetch');

const self = module.exports = {

	generateSecret: data => {
		if (Array.isArray(data))
			return self.generateHashFor(data.join('-') + env.SECRET);

		if (data && typeof data === 'object')
			return self.generateHashFor(JSON.stringify(data) + env.SECRET);
	},

	generateHashSyncFor: value => bcrypt.hashSync(value, 10),

	generateHashFor: value => bcrypt.hash(value, 10),

	verifyHash: (value, hash) => bcrypt.compare(value, hash),

	verifyHashSync: (value, hash) => bcrypt.compareSync(value, hash),

	generateBase64Secret: data => {
		if (typeof data === 'string')
			data = `${data}&@&${env.SECRET}`

		if (Array.isArray(data))
			data = data.join('-');

		if (typeof data === 'object')
			data = JSON.stringify(data);

		return Buffer.from(data).toString('base64');
	},

	decodeBase64Secret: secret => Buffer.from(secret, 'base64').toString('ascii'),

	generateAuthenticateId: async(access_token, type) => {
		try{
			let uri;
			if(type == 'facebook')
					uri = `https://graph.facebook.com/v2.8/me?access_token=${access_token}`;
			else if(type == 'google')
				uri = `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`;
			
			let auth = await fetch(uri).then(res=> res.json());
			return auth;
		}catch(err){
			console.log('Errrrror', err)
		}
	}
};