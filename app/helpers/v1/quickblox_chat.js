"use strict";
const env = require('./../../../config/environments');

const _hash = require('./../../helpers/v1/hash');
const _err = require('./../../helpers/v1/error');

const __mailgun = require('./../../services/v1/mailgun');
const __QB = require('./../../services/v1/quickblox');

const mailTemplate = require('../../../config/mailer/templates');

const CHAT_MESSAGE = {
    welcome_text: 'Hi, welcome to UniRely chat. I am your advisor and please feel free to ask me any questions related to applying abroad.',
    university_generate_list: 'A counselor is working on your list. You will receive your tentative university list within 24 hours.'
};

module.exports = {

    async adminMessage(data, adminQB, messageType){
        try{
            if(CHAT_MESSAGE[messageType]){
                let connect =  await __QB.connectChat(adminQB.id, adminQB.password);
                let message = await __QB.chatMessage(data.id, CHAT_MESSAGE[messageType], 'chat'); 
                await __QB.disconnectChat();
                return connect;
            }
            else throw _err.createError('Bad Request', 'Not found message with provided type' );
        } catch(error){
            throw error;
        }
    },

    sendChatNotificationEmail (data) {
    	try {
            const link = env.APP_URL;

		    const emailData = {
			    to: data.to,
			    from: env.ADMIN_LOGIN,
			    subject: 'Chat Notification',
			    html: mailTemplate.get('chat-notification', { link, user: data.name, message: data.message, receiver: data.sender })
            };
		    return __mailgun.sendMail(emailData);
	    }
	    catch(err) { console.log(err); }
    },
}