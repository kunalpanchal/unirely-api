"use strict";
const mongoose = require('mongoose');
const UniversityApplication = mongoose.model('UniversityApplication');

const _err = require('../error');
const _sharedValidator = require('./shared_validator');
module.exports = {

	isValidUniversityApplication ({ user, universities }) {
		if (!user || !universities || !Array.isArray(universities) || universities.length === 0)
			throw _err.createError(
				'INVALID_APPLICATION',
				`Application either doesn't contain required fields or is malformed`
			);
		return true;
	},

	isValidUniversityApplicationUpdate({user, connection_ids, new_universities}){
		if(!user || !connection_ids || !new_universities || !_sharedValidator.array_validator([connection_ids, new_universities]))
			throw _err.createError(
				'INVALID_APPLICATION',
				`Application either doesn't contain required fields or is malformed`
			);
		return true;
	},

	/*async areNewApplications({ user, universities }) {
		const existingApplications = await UniversityApplication.find({ user, university: { $in: universities }}).populate('university').lean();
		if (existingApplications.length)
			throw _err.createError(
				'DUPLICATE_RESOURCE',
				`Already applied for ${existingApplications.map(application => application.university.name).join(' ')}`
			);

		return true;
	},*/

	isValidMentorAssignRequest ({ mentor, application }) {
		if (!mentor || !application)
			throw _err.createError(
				'BAD_REQUEST',
				`Either mentor or application id is missing from request`
			);

		return true;
 	},

	isValidcounselorAssignRequest ({ counselor, student }) {
		if (!counselor || !student)
			throw _err.createError(
				'BAD_REQUEST',
				`Either counselor or student id is missing from request`
			);

		return true;
	},

	isValidApplication (application) {
		if (!application)
			throw _err.createError(
				'RESOURCE_NOT_FOUND',
				`Requested application was not found`
			);
		return true;
	}
};