"use strict";

const mongoose = require('mongoose');
const ChatRoom = mongoose.model('ChatRoom');
const _err = require('./../error');

module.exports = {
    isValidUserRoomCreationRequest({ dialog_ids }) {
        if (!dialog_ids || !dialog_ids.length)
            _err.createError('BAD_REQUEST', 'Dialog id is required to create a chatroom for user' );

        return true;
    },

    isValidChatRoomRequest(data){
        let errorData = Object.keys(data).filter(key=> !data[key]);
        if(errorData.length > 0)
            _err.createError('BAD_REQUEST', `${errorData.join()} is required to create a chatroom for user` );
        
        return true;
    },

    async isUserNewToRooms (userId, roomIds) {
        let existingRooms = await ChatRoom.find({ dialog_id: { $in: roomIds } });
        for (let room of existingRooms) {
            if (room.users.indexOf(userId) >= 0)
                throw _err.createError('DUPLICATE_RESOURCE', 'User already registered in the mentioned chat room');
        }
        return true;
    },

    async isValidRoomUserRegistrationRequest ({ users }) {
        if (!users || !users.length)
            throw _err.createError('BAD_REQUEST', 'Users are required to be registered into chat room' );

        return true;
    },

    async areUsersNewToRoom (dialog_id, users) {
        if (!dialog_id || !users)
            throw _err.createError('INTERNAL_SERVER_ERROR', 'Could not process new room user validation');

        let existingRoomUsers = await ChatRoom.findOne({ dialog_id, users: { $in: users } });

        if (existingRoomUsers)
            throw _err.createError('DUPLICATE_RESOURCE', 'User already registered in the mentioned chat room');
        return true;
    }
};