"use strict";
const mongoose = require('mongoose');
const Plan = mongoose.model('Plan');

const _err = require('../error');

module.exports = {

    isValidUniversity(university_name){
        if(!university_name)
            throw _err.createError(
				'INVALID_APPLICATION',
				`Application either doesn't contain required fields or is malformed`
            );
        return true;
    }
}