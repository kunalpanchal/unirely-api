"use strict";

const mongoose = require('mongoose');
const User = mongoose.model('User');

const _err = require('../error');
const _hash = require('../hash');
const _hashValidator = require('./hash');

module.exports = {

	async isNewUser(username) {
		let user = await User.findOne({ 'email': username });

		if (user)
			throw _err.createError('DUPLICATE_RESOURCE', 'User already registered');

		return !false;
	},

	isValidUser(user) {
		if (!user)
			throw _err.createError('RESOURCE_NOT_FOUND', 'User not found');

		return true;
	},

    async areValidUsers(userIds) {
	    if (!userIds || !userIds.length)
	        throw _err.createError('BAD_REQUEST', 'Users are not provided in request body' );

	    let users = await User.find({ _id: { $in: userIds }});
	    if (users.length !== userIds.length)
	        throw _err.createError('RESOURCE_NOT_FOUND', 'User nor found for one of the provided ids' );

	    return true;
    },

	isValidOldPassword(oldPassword, user) {
		if (oldPassword != user.password)
			throw _err.createError('CREDENTIALS_INCORRECT', 'Old password does not match');
		return true;
	},

	isValidUserPassword(user, password) {
		if (!password)
			throw _err.createError('CREDENTIALS_INCORRECT', 'User password cannot be empty');

		if (!_hash.verifyHashSync(password, user.password))
			throw _err.createError('CREDENTIALS_INCORRECT', 'User password does not match');

		return true;
	},

	async isValidUserAuthenticate(user, payload){
		if (!payload.authenticate.type || !payload.authenticate.id)
			throw _err.createError('CREDENTIALS_INCORRECT', 'User authenticate type/id cannot be empty');

		let user_accounts = user.authenticate ? user.authenticate.find(auth=> auth.type == payload.authenticate.type) : false;

		console.log(user_accounts,'.................');
		if (!user_accounts){
			let generateAuth = await _hash.generateAuthenticateId(payload.password, payload.authenticate.type);
			if(generateAuth.id == payload.authenticate.id)
				await User.update({_id: user._id},	{	$push: {authenticate: payload.authenticate} });
			else 
			throw _err.createError('CREDENTIALS_INCORRECT', 'User authenticate failed');
		}else if(user_accounts.id != payload.authenticate.id)
				throw _err.createError('CREDENTIALS_INCORRECT', 'User authenticate failed');

		return true;
	},

	isValidArry(data){
		if(data == null || data.length<= 0)
			throw _err.createError('EMPTY_DATA', 'Empty university list')

		return true;
	},
	isValidRegisterRequest(reqData) {
		if (!reqData.username)
			throw _err.createError('BAD_REQUEST', 'username is required');

		if (!reqData.password)
			throw _err.createError('BAD_REQUEST', 'password is required');

		if (!reqData.roles)
			throw _err.createError('BAD_REQUEST', 'roles are required');

		return true;
	},

	isValidResetFlowOneRequest(data) {
		if (!data.email)
			throw _err.createError('BAD_REQUEST', 'email is required in request params');

		return true;
	},

	areValidResetFlowTwoParams(email, hash) {
		if (!email || !hash)
			throw _err.createError('BAD_REQUEST', 'Too few parameters supplied');
		return true;
	},

    isValidAccountVerificationRequest(email, hash) {
	    if (!email || !hash)
	        throw _err.createError('BAD_REQUEST', 'Too few parameters supplied' );

	    return true;
    },

	isValidVerificationHash(user, hash) {
		if (user.verification_hash !== hash)
			throw _err.createError('BAD_REQUEST', 'Invalid verification hash');

		return true;
	},

    isAdmin (user) {
	    return user.roles.some(role => role.name === 'ADMIN');
	}
	
};