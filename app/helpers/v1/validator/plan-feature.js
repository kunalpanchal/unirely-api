"use strict";
const mongoose = require('mongoose');
const Plan = mongoose.model('Plan');

const _err = require('../error');

module.exports = {

    isValidPlan(plan_id, name){
        if(!plan_id || !name)
            throw _err.createError(
				'INVALID_APPLICATION',
				`Application either doesn't contain required fields or is malformed`
            );
        return true;
	},

    async isPlanExist(plan_id){
        let plan = await Plan.findById(plan_id);
        if(!plan)
            throw _err.createError('RESOURCE_NOT_FOUND', 'Plan not found for one of the provided plan id' );
        return true;
    },

	isValidFeature(feature_id, name){
        if(!feature_id || !name)
            throw _err.createError(
				'INVALID_APPLICATION',
				`Application either doesn't contain required fields or is malformed`
            );
        return true;
    },
	
}