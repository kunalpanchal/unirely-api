"use strict";

const mongoose = require('mongoose');
const Role = mongoose.model('Role');

const _err = require('./../error');

module.exports = {

	areValidRoles (roles) {
		if (Array.isArray(roles) && !roles.length)
			throw _err.createError('RESOURCE_NOT_FOUND', 'Roles not found');

        if (!roles)
            throw _err.createError('RESOURCE_NOT_FOUND', 'Role not found');
		return true;
	},

	userHasRole (user, roleName) {
		if (!user || !roleName)
			throw _err.createError('RESOURCE_NOT_FOUND', 'User or role missing');

		if (user.roles.some(role => role.name === roleName))
			return true;

		throw _err.createError('BAD_REQUEST', `User does not belong to ${roleName}s`);
	}
}