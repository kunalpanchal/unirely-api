"use strict";

const _hash = require('./../../../helpers/v1/hash');
const _err = require('./../error');

module.exports = {
    hashHasNotExpired (hash) {
        let decodedHash = JSON.parse(_hash.decodeBase64Secret(hash));
        let expiresOn = decodedHash.iat + decodedHash.expiresIn * 1000;
        let now = Date.now();

        if (now > expiresOn)
            throw _err.createError('BAD_REQUEST', 'Verification hash has expired' );

        return true;
    }
};