"use strict";

module.exports = {
	oauth: require('./oauth'),
	user: require('./user'),
	role: require('./role'),
    hash: require('./hash'),
    mailer: require('./mailer'),
    chat: require('./chat'),
	uniApplication: require('./university-application'),
	planFeature: require('./plan-feature'),
	university: require('./university')
};