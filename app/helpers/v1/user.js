'use strict';
const mongoose = require('mongoose');
const User = mongoose.model('User');
const University = mongoose.model('University');
const Subscription = mongoose.model('Subscription');

const _hash = require('./hash');
const _validator = require('./validator');
const _error = require('./error');

const ChargebeeCustomer = require('../../services/chargebee/customer');
const ChargebeeSubscription = require('../../services/chargebee/subscription');

module.exports = {
    async studentUpdateDetails(user = null, id, data) {
        if (id)
            return await User.findOneAndUpdate({ _id: id }, data, { new: true });
    },
    async changePassword(user = null, id, data) {
        if (id) {
            let user = await User.findOne({ _id: id });
            console.log('Users Done', user);
            if (_validator.user.isValidUser(user)) {
                if (_hash.verifyHashSync(data.oldPassword, user.password)) {
                    user.password = data.password;
                    return await user.save();
                }
                else {
                    console.log('In Else');
                    throw _error.createError(_error.getError('CREDENTIALS_INCORRECT'), 'Old password does not match');
                }
            }
        }
    },

    async forgetPassword(user = null, id, data) {
        if (id) {
            let user = await User.findOne({ _id: id });
            if (_validator.user.isValidUser(user)) {
                user.password = data.password;
                return awaituser.save();
            }
        }
    },

    async universityDetails(searchValue) {
        console.log(searchValue,'Searcg')
        if(!searchValue || searchValue == "")  {
            return await University.find({priority: true}).limit(9);
        }
        else if(searchValue == "*") return await University.find();
        else {
            searchValue = new RegExp(searchValue, 'gi')
            return await University.find({name: searchValue});
        }
    },

    async userDetails(id) {
        let user = await User.findOne({ '_id': id }).populate({ path: 'universities' });
        if (_validator.user.isValidUser(user))
            return user;
    },

    async updateDetails(id, data) {
        try{
            return await User.findOneAndUpdate({ _id: id }, data, { new: true });
        } catch(error){
            throw error;
        }
    },

    async uploadImage(id, data) {
        return await User.findOneAndUpdate({_id: id}, {$set: {metas: data}}, {new: true});
	},

	async subscription(user){
		let chargebee_customer = await ChargebeeCustomer.create(user); 
        
		let chargebee_subscription = await ChargebeeSubscription.create(chargebee_customer.id, 'basic');

        console.log(chargebee_customer, '******************', chargebee_subscription)
		let subscription = await Subscription.create(
				{	
					chargebee_customer_id: chargebee_customer.id,
					user: user._id,
					chargebee_subscription_id:  chargebee_subscription.subscription.id,
                    chargebee_plan_id: 'basic',
                    current_usage:{
                        university: 0
                    }
				}
			);
		return subscription;
	}
}