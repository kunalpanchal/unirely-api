'use strict';
const mongoose = require('mongoose');
const User = mongoose.model('User');
const University = mongoose.model('University');

const env = require('../../../config/environments');
const __mailgun = require('./../../services/v1/mailgun');
const _error = require('./error');

const mailTemplate = require('../../../config/mailer/templates');

module.exports = {
   
    approveRejectNotifyAdmin(data){
        try {
            const link = env.APP_URL;

		    const emailData = {
			    to: data.to,
			    from: data.from,
			    subject: 'Approve Reject Notify',
			    html: mailTemplate.get('approve-reject-notification-admin', { status: data.status, student: data.user.email, university: data.university.name })
			};
			console.log('******************Mail Send***************', emailData)
		    return __mailgun.sendMail(emailData);
	    }
	    catch(err) { console.log(err); }
	},
	
	generateListNotifyAdmin(data){
        try {
			console.log(data)
            const emailData = {
			    to: data.to,
			    from: data.from,
			    subject: 'University Generate List Notify',
			    html: mailTemplate.get('generate-list-notification-admin', {user: data.from })
			};
			console.log('******************Mail Send***************', emailData)
		    return __mailgun.sendMail(emailData);
	    }
	    catch(err) { console.log(err); }
	},
}