"use strict";
const envConfig = require('./../../../config/environments');
const _err = require('./../../helpers/v1/error');

const __QB = require('./../../services/v1/quickblox');

module.exports = {
    async createQuickbloxDialog(data, admin){
        try{
            await __QB.createUserSession(admin.login, admin.password);
            let dialog_data = {
                opponent_ids: data.ids,
                name: data.name
            };
            const qbDialog = await __QB.createDialog(dialog_data, 'group_chat');
            return qbDialog;
        }catch(err){
            console.log(err);
            throw _err.createError('QUICKBLOX_ERROR', err.message );
        }
    }
}