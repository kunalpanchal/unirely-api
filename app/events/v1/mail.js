"use strict";

const _ = require('lodash');
const eventEmitter = require('./../event');
const _oauth = require('./../../helpers/v1/oauth');
const _chat = require('./../../helpers/v1/quickblox_chat');
const _university = require('./../../helpers/v1/university');

const _err = require('./../../helpers/v1/error');
const { logger } = require('../../../config/logger');

eventEmitter.on('SIGNEDUP', async function (data) {
    try {
        if (Array.isArray(data)) {
            data = _.head(_.filter(data, { 'action': 'SEND_ACCOUNT_VERIFICATION_MAIL' }));
        }

        if (data && data.action === 'SEND_ACCOUNT_VERIFICATION_MAIL') {
            const response = await _oauth.sendAccountVerificationEmail(data.data.hash, data.data.to, data.data.from, data.data.role);
        }
    }
    catch(err) {
        logger.error(err);
    }
});

eventEmitter.on('PASSWORD_RESET_REQUEST', async function (data) {
    try {
        if (data && data.action === 'SEND_PASSWORD_RESET_MAIL') {
            const response = await _oauth.sendPasswordResetMail(data.data.hash, data.data.to, data.data.from, data.data.receiver);
        }
    }
    catch(err) {
        logger.error(err);
    }
})

eventEmitter.on('CHAT_NOTIFY', async function (data) {
    try {
        if (data && data.action === 'EMAIL_NOTIFICATION_REQUEST') {
            const response = await _chat.sendChatNotificationEmail(data.data);
        }
    }
    catch(err) {
        logger.error(err);
    }
})

eventEmitter.on('APPROVE_REJECT', async function (data) {
    try {
        if (data && data.action === 'SEND_APPROVE_REJECT_NOTIFY_ADMIN') {
            const response = await _university.approveRejectNotifyAdmin(data.data);
        }
    }
    catch(err) {
        logger.error(err);
    }
})

eventEmitter.on('GENERATE_LIST', async function (data) {
    try {
        if (data && data.action === 'SEND_NOTIFICATION_TO_ADMIN') {
            const response = await _university.generateListNotifyAdmin(data.data);
        }
    }
    catch(err) {
        logger.error(err);
    }
})