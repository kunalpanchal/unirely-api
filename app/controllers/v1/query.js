"use strict";

const mongoose = require('mongoose');
const User = mongoose.model('User');

const response = require('../../middlewares/response');

const _validator = require('../../helpers/v1/validator');
const _err = require('../../helpers/v1/error');

module.exports = {
    async userStudentDetails (req, res) {
        let users = await User.find({email:{$ne: 'management@unirely.com'}}).select(' metas student_details');
        let updateBatch = users.filter(user => !user.student_details || !user.student_details.full_name)
        .map(user => {

            let fullName = user.metas.reduce(function (res, it) {
                if (it.key === "first_name" || it.key === "last_name") {
                    res += it.value ? it.value+" " : "";
                }
                return res;
            }, "");
            user.student_details = user.student_details ? Object.assign(user.student_details, { full_name: fullName.trim() }) : { full_name: fullName.trim() };
            return user.save();
        });

        await Promise.all(updateBatch);
        response.ok(res, users);
    },

    async userVerification (req, res) {
        let users = await User.find({email:{$ne: 'management@unirely.com'}}).select('verified');
        let updateBatch = users.map(user => {
            if(user.verified == 'false' || user.verified == 'true'){
                user.verified = user.verified == 'true' ? 'APPROVED' : 'REJECTED';
                return user.save();
            }
        })
        await Promise.all(updateBatch);
        response.ok(res, users);
    },
}