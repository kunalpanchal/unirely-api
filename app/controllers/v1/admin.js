"use strict";

const _ = require('lodash');
const env = require('../../../config/environments');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Role = mongoose.model('Role');
const University = mongoose.model('University');
const UniversityUsers = mongoose.model('UniversityUsers');
const UniversityApplication = mongoose.model('UniversityApplication');
const ChatRoom = mongoose.model('ChatRoom');

const response = require('../../middlewares/response');

const _validator = require('../../helpers/v1/validator');
const _err = require('../../helpers/v1/error');
const _chat = require('../../helpers/v1/chat');

const ChargebeeAddon = require('../../services/chargebee/addon');
module.exports = {
	async getAllStudents(req, res) {
		const role = await Role.findOne({ name: 'STUDENT' });
		const filter = {
			university_generate_form: {'student_details.university_generate_form': true} 
		};
		if (req.body.dataTableAttr && req.body.dataTableAttr !== null) {
			let page_skip = req.body.dataTableAttr.page * req.body.dataTableAttr.limit;
			let limit = req.body.dataTableAttr.limit;
			let searchKey = req.body.dataTableAttr.searchKey;
			let filterKey = req.body.dataTableAttr.filter;

			let project = {'email':1, 'metas':1, 'createdAt':1, 'student_details':1 };
			let query = [{$match: {roles: role._id}}];

			switch(filterKey){
				case 'university_generate_form': query = [{$match: {$and: [{'roles': role._id}, {'student_details.university_generate_form': true}] }	}]; 
												break;
				case 'paid': query = [
										{$match: {'roles': role._id}	},
										{$lookup: { from: 'subscriptions', localField: '_id', foreignField: 'user', as: 'subscription'} },
										{$unwind: '$subscription'},
										{$match: {'subscription.invoice.1': {$exists: true} }	},
									];
									project = Object.assign(project, {subscription:1});
									break;
				case 'free': query = [
								{$match: 	{'roles': role._id}	},
								{$lookup: { from: 'subscriptions', localField: '_id', foreignField: 'user', as: 'subscription'} },
								{$unwind: '$subscription'},
								{$match: {'subscription.invoice.1': {$exists: false} }	},
							];
							project = Object.assign(project, {subscription:1})  
							break;
				default: break;
			}

			if (searchKey != '') {
				let regexQ = {$regex: searchKey, $options: "i"};
				query.push({$match: {'student_details.full_name': regexQ}		});
			}
			
			const students = await User.aggregate(query).skip(page_skip).limit(limit).project(project);
			
			const totalCount = await User.aggregate(	[...query, {$group: {_id: null, count: {$sum: 1}}} ]	);

			return response.ok(res, { students, totalCount: totalCount[0].count });
		}

		const students = await User.find({ roles: role._id }).select('email metas createdAt');
		return response.ok(res, students);
	},

	async getAllMentors(req, res) {
		try {
			const role = await Role.findOne({ name: 'MENTOR' });

			if (req.body.dataTableAttr && req.body.dataTableAttr !== null) {
				let page_skip = req.body.dataTableAttr.page * req.body.dataTableAttr.limit;
				let limit = req.body.dataTableAttr.limit;
				let searchKey = req.body.dataTableAttr.searchKey;
				let filterKey = req.body.dataTableAttr.filter;
	
				let query = {roles: role._id};


				if (searchKey != '') {
					let regexQ = {$regex: searchKey, $options: "i"};
					query = {$and: [ {'student_details.full_name': regexQ},{roles: role._id} ] };
				}

				const mentors = await User.find(query).select('email verified metas createdAt student_details').lean();
				const totalCount = mentors ? mentors.length : 0;

				const mentorIds = mentors.map(mentor => mentor._id);
				
				const mentorUniversities = await UniversityUsers.find({ user: { $in: mentorIds } }).populate('university').lean();
	
				const result = mentors.map(mentor => Object.assign({}, mentor, {
					university: Object.assign({}, ...mentorUniversities
						.filter(uni => uni.user.toString() === mentor._id.toString())
						.map(uni => uni.university)
					)
				}));
				return response.ok(res, { mentors, totalCount });
			}
		}
		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async getAllCounselors(req, res) {
		try{
			const role = await Role.findOne({ name: 'COUNSELOR' });
			if (req.body.dataTableAttr && req.body.dataTableAttr !== null) {
				let page_skip = req.body.dataTableAttr.page * req.body.dataTableAttr.limit;
				let limit = req.body.dataTableAttr.limit;
				let searchKey = req.body.dataTableAttr.searchKey;
				let filterKey = req.body.dataTableAttr.filter;

				let query = {roles: role._id};


				if (searchKey != '') {
					let regexQ = {$regex: searchKey, $options: "i"};
					query = {$and: [ {'student_details.full_name': regexQ},{roles: role._id} ] };
				}

				const counselors = await User.find({ roles: role._id }).select('email verified metas createdAt student_details');
				const totalCount = counselors ? counselors.length : 0;
				return response.ok(res, { counselors, totalCount });
			}
			const counselors = await User.find({ roles: role._id }).select('email verified metas createdAt student_details');
			return response.ok(res, counselors);
		}catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async getUniversityMentors(req, res) {
		try {
			const university = await University.findById(req.params.id);
			if (!university)
				throw _err.createError('RESOURCE_NOT_FOUND', 'university not found');

			const uniMentors = await UniversityUsers
				.find({ university: req.params.id, type: 'MENTOR' })
				.populate('user', 'email metas roles quickblox')
				.populate('university', 'name contact address description image')
				.lean();
			response.ok(res, uniMentors);
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async getUniversitiesMentors(req, res) {
		try {
			if (!req.body.universities || !Array.isArray(req.body.universities) || !req.body.universities.length)
				throw _err.createError('BAD_REQUEST');

			const uniMentors = await UniversityUsers
				.find({ university: { $in: req.body.universities }, type: 'MENTOR' })
				.populate('user', 'email metas roles quickblox')
				.populate('university', 'name contact address description image')
				.lean();

			console.log(uniMentors);

			const responseData = _.groupBy(uniMentors, 'university._id');
			response.ok(res, responseData);
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async approveMentor(req, res) {
		try {
			let message = "";
			if (!req.params.id)
				throw _err.createError('BAD_REQUEST', 'No mentor id present');
			if (!req.params.status)
				throw _err.createError('BAD_REQUEST', 'No mentor status present');

			const mentor = await User.findById(req.params.id).populate('roles');

			if (_validator.user.isValidUser(mentor) && _validator.role.userHasRole(mentor, 'MENTOR')) {
				[mentor.verified, message] = req.params.status == 'approve' ? ['APPROVED', 'Approved Successfully'] : ['REJECTED', 'Rejected Successfully']
				await mentor.save();
				return response.modified(res, Object.assign(mentor, {
					message: message
				})
				);
			}
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async approvecounselor(req, res) {
		try {
			if (!req.params.id)
				throw _err.createError('BAD_REQUEST', 'No counselor id present');

			const counselor = await User.findById(req.params.id).populate('roles');

			if (_validator.user.isValidUser(counselor) && _validator.role.userHasRole(counselor, 'counselor')) {
				counselor.verified = true;
				await counselor.save();
				return response.modified(res, {
					message: 'Approved successfully'
				});
			}
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async assignMentorToApplication(req, res) {
		try {
			if (_validator.uniApplication.isValidMentorAssignRequest(req.params)) {
				const mentor = await User.findById(req.params.mentor);
				const application = await UniversityApplication.findById(req.params.application);
				if (
					_validator.user.isValidUser(mentor) &&
					_validator.uniApplication.isValidApplication(application)
				) {
					application.mentor.person = mentor._id;
					application.mentor.status = 'PENDING';
					await application.save();
					response.modified(res, {
						message: 'Assigned mentor to application',
						application
					});
				}
			}
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async assignMentorsToApplications(req, res) {
		try {
			const applicationMentors = req.body.applications;
			if (
				!Array.isArray(applicationMentors) ||
				applicationMentors.some(ob => !ob.application_id || !ob.mentor_id) ||
				!req.body.counselor ||
				!req.body.user
			)
				throw _err.createError('BAD_REQUEST', 'Either not an array of application or mentor/counselor/user missing');

			// Mentor-Counselor Quickblox Dialog
				// let quickbloxApplication = await User.find({ _id: { $in: [applicationCounselor, ...applicationMentors.map(ob => ob.mentor_id)] } }).select('quickblox');
				// let admin = await User.findOne({ email: env.ADMIN_LOGIN });
				// let counselorQb = quickbloxApplication.find(ob => ob._id == applicationCounselor)
				// let quickbloxArray = quickbloxApplication.filter(ob => ob._id != applicationCounselor)
				// 	.map(async ob => {
				// 		let data = {
				// 			ids: [ob.quickblox.id, counselorQb.quickblox.id],
				// 			name: `${ob.quickblox.id}-${counselorQb.quickblox.id}`
				// 		}
				// 		let groupDialog = await _chat.createQuickbloxDialog(data, admin.quickblox);
				// 		return await ChatRoom.create(
				// 			{
				// 				dialog_id: groupDialog._id,
				// 				group_id: groupDialog.xmpp_room_jid,
				// 				users: [admin._id, applicationCounselor, ob._id],
				// 				room_name: `mentor-counselor`
				// 			}
				// 		);
				// 	});
				// Promise.all(quickbloxArray);
			// End

			let applications = await UniversityApplication
				.find({ _id: { $in: applicationMentors.map(ob => ob.application_id) }})

			let counselorApplication = await UniversityApplication.findOne({$and: 
                [ 
                    {'counselor.person': req.body.counselor} , {status: 'ACTIVE'}, {'counselor.status': 'ACCEPTED'}, {'user': req.body.user}
                ]
            });

			let updatePromise = [];
			for (let application of applications) {
				let apMentorOb = applicationMentors.find(ob => ob.application_id === application.id);
				application.mentor.person = apMentorOb.mentor_id;
				
				if(counselorApplication)	application.counselor = counselorApplication.counselor;

				updatePromise.push(application.save());
			}
			let updatedData = await Promise.all(updatePromise);
			response.modified(res, updatedData);
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	/*async assigncounselorToApplication (req, res) {
		try {
			if (_validator.uniApplication.isValidcounselorAssignRequest(req.params)) {
				const counselor = await User.findById(req.params.counselor);
				const application = await UniversityApplication.findById(req.params.application);
				if (
					_validator.user.isValidUser(counselor) &&
					_validator.uniApplication.isValidApplication(application)
				) {
					application.counselor.person = counselor._id;
					application.counselor.status = 'PENDING';
					await application.save();
					response.modified(res, {
						message: 'Assigned counselor to application',
						application
					});
				}
			}
		}

		catch(err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},*/

	async assigncounselorToApplications(req, res) {
		try {
			if (_validator.uniApplication.isValidcounselorAssignRequest(req.params)) {
				const counselor = await User.findById(req.params.counselor);
				const application_ids = await UniversityApplication.find({ user: req.params.student }).select('_id');
				if (
					_validator.user.isValidUser(counselor) &&
					application_ids.length > 0
				) {
					console.log(counselor._id);
					let update = await UniversityApplication.updateMany({ _id: { $in: application_ids } }, {
						$set: {
							counselor: {
								person: counselor._id,
								status: 'PENDING'
							}
						}
					});
					response.modified(res, {
						message: 'Assigned counselor to application',
					});
				}
			}
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async fetchAllApplications(req, res) {
		try {
			const applications = await UniversityApplication.find({})
				.populate('user', 'email metas')
				.populate('admin', 'email')
				.populate('university', 'name')
				.populate('mentor.person', 'email')
				.populate('counselor.person', 'email');

			let responseData = [];
			applications.forEach(application => {
				let existingData = responseData.filter(data => data.user._id === application.user._id)[0];

				if (!existingData)
					existingData = {};

				if (!existingData.connections)
					existingData.connections = [];

				existingData.connections.push({ university: application.university, mentor: application.mentor });

				if (!existingData.application_id) {
					existingData.application_id = application._id;
					existingData.user = application.user;
					existingData.admin = application.admin;
					responseData.push(existingData);
				}
			});
			response.ok(res, responseData);
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async updateUniversityApplication(req, res) {
		try {
			const admin = await User.findOne({ email: 'management@unirely.com' });
			if (
				_validator.uniApplication.isValidUniversityApplicationUpdate(req.body) &&
				_validator.user.isValidUser(admin)
			) {
				let connection_ids = req.body.connection_ids;
				let new_universities = req.body.new_universities;
				connection_ids = await UniversityApplication.updateMany({ user: req.body.user, status: "ACTIVE", university: { $in: connection_ids } }, { "status": "IN_ACTIVE" });
				const existingApplications = await UniversityApplication
					.find({ user: req.body.user, status: 'ACTIVE', university: { $in: new_universities } })
				if (existingApplications.length === 0) {
					const application_university = new_universities.map(university => Object.assign({}, { university, user: req.body.user, admin: admin._id }));
					let result = await UniversityApplication.insertMany(application_university);
					return response.created(res, result);
				}
				throw _err.createError('DUPLICATE_RESOURCE', 'Universities have already been applied');
			}
		}
		catch (err) {
			response.error(res, err);
		}
	},

	async universityUpdate(req, res) {
		try {
			if (_validator.university.isValidUniversity(req.body.name)) {
				let addon = await ChargebeeAddon.create({ name: req.body.name, price: req.body.price });
				Object.assign(req.body, { addon_id: addon.addon.id });
				let university = await University.findOneAndUpdate({ name: req.body.name }, req.body, { upsert: true, returnNewDocument: true, new: true });
				response.ok(res, university);
			}
		}
		catch (error) {
			console.log(error);
			if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
		}
	}
};