'use strict';

const mongoose = require('mongoose');
const Plan = mongoose.model('Plan');

const response = require('../../middlewares/response');

const _err = require('../../helpers/v1/error');
const _validator = require('../../helpers/v1/validator');

const ChargebeePlan = require('../../services/chargebee/plan');
module.exports = {

    async create(req, res){
        try{
            const plan_body = {
                _id: req.body.plan_id,
                name: req.body.name,
                description: req.body.description,
                active: req.body.active,
				price: !isNaN(req.body.price) ? req.body.price : undefined,
				meta_data: req.body.meta
			}
            if(
                _validator.planFeature.isValidPlan(plan_body._id,plan_body.name)
            ){
                let chargebee_plan = await ChargebeePlan.create(plan_body);
                let plan_response = await Plan.create(plan_body);
                response.created(res, plan_response); 
            }
        }
        catch(error){
            if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
        }
	},

	async update(req, res){
        try{
			const plan_id = req.params.plan;
            const plan_body = {
                name: req.body.name,
                description: req.body.description,
                active: req.body.active,
				price: req.body.price && !isNaN(req.body.price) ? req.body.price : undefined,
				meta_data: req.body.meta
			}
			console.log(plan_body);
            if(
                _validator.planFeature.isValidPlan(plan_id, plan_body.name)
            ){
				let chargebee_plan = await ChargebeePlan.update(plan_id, plan_body);
                let plan_response = await Plan.findByIdAndUpdate(plan_id, plan_body, {new: true});
                response.modified(res, plan_response); 
            }
        }
        catch(error){
            if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
        }
	},

	async fetch(req, res){
		try{
			console.log("************",req.params.plan)
			if(!req.params.plan)
			throw _err.createError('BAD REQUEST', 'Request params not found');

			let planId = req.params.plan == 'all' ? "" : req.params.plan;
			let plan = await Plan.find({_id: new RegExp(planId+"$")});
				response.ok(res, plan);
		}
		catch(error){
			if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
		}
	},

	async delete(req, res){
		try{
			const plan_id = req.params.plan_id;
			if(plan_id){
                let chargebee_plan = await ChargebeePlan.delete(plan_id);
                let plan_response = await Plan.findByIdAndRemove(plan_id);
                response.deleted(res, plan_response); 
            }
			response.ok(res, plan);
		}
		catch(error){
			if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
		}
	},
	
	async featureAccessByPlan(req,res){
		try{
			if(!req.params.plan)
				throw _err.createError('BAD REQUEST', 'Request params not found');

            let plan = await Plan.findById(req.params.plan).select('university_limit')
            response.ok(res, plan);
		}catch(error){
			if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
		}
	}
}

