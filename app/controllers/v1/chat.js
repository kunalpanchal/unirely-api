"use strict";

const mongoose = require('mongoose');
const env = require('../../../config/environments');
const User = mongoose.model('User');
const ChatRoom = mongoose.model('ChatRoom');
const UniversityApplication = mongoose.model('UniversityApplication');

const response = require('../../middlewares/response');

const _err = require('../../helpers/v1/error');
const _validator = require('../../helpers/v1/validator/index');
const EventData = require('../../interfaces/event-data');
const eventEmitter = require('../../events/event');

const _chat = require('../../helpers/v1/chat');
module.exports = {

    async getUserRooms(req, res) {
        try {
            const userId = req.params.userId;
            const rooms = await ChatRoom.find({ users: userId }).populate('users', 'email metas quickblox student_details');


            const applications = await UniversityApplication
            .find({ $and: [ {chat_room: { $in: rooms.map(room => room._id)} },  {'mentor.status': 'ACCEPTED'} ]})
            .select('university chat_room status')
            .populate('university')
            .lean();

            let responseOb = {
                user: userId,
                rooms: rooms.map(room => {
                    return Object.assign({}, {
                        room_id: room._id,
                        status: room.status,
                        dialog_id: room.dialog_id,
                        group_id: room.group_id,
                        room_name: room.room_name,
                        occupants: room.users.filter(user => user._id.toString() !== userId.toString()),
                        application:  applications.find(application=> application.chat_room.find(chat => chat.toString() == room._id.toString()) ) 
                    });
                })
            };

            response.ok(res, responseOb);
        }
        catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async registerUserToRooms(req, res) {
        try {
            const userId = req.params.userId;
            const user = await User.findById(userId);
            if (
                _validator.user.isValidUser(user) &&
                _validator.chat.isValidUserRoomCreationRequest(req.body) &&
                await _validator.chat.isUserNewToRooms(req.params.userId, req.body.dialog_ids)
            ) {
                let chatRoomData = [];
                for (let dialog_id of req.body.dialog_ids) {
                    let data = await ChatRoom.findOneAndUpdate(
                        { dialog_id, group_id: req.body.group_id },
                        { $push: { users: userId } },
                        { upsert: true, new: true }
                    );
                    chatRoomData.push(data);
                }
                response.created(res, chatRoomData);
            }
        }

        catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async registerChatRoom(req, res){
        try {
            const userId = req.params.userId;
            const user = await User.findById(userId);
            let data = {
                dialog_id: req.body.dialog_id,
                group_id: req.body.group_id,
                room_name: req.body.room_name,
                users: req.body.users
            };
            if (
                _validator.user.isValidUser(user) &&
                _validator.chat.isValidChatRoomRequest(data)
            ) { 
                    data.users.push(userId);                
                    let chatRoom =  await ChatRoom.create(data);
                    
                    console.log('.....................Chec', chatRoom)
                    await mentorCounselorDialogCreate(chatRoom);
                    response.created(res, chatRoom);
                }
        }
        catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async getRoomUsers(req, res) {
        try {
            const dialog_id = req.params.dialogId;
            let rooms = await ChatRoom.findOne({ dialog_id }).populate('users', 'email metas');

            response.ok(res, rooms);
        }
        catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async registerUsersToRoom(req, res) {
        try {
            console.log(req.body.room_name, '>>>>>>>>>>>>Room Name');
            const dialog_id = req.params.dialogId;
            if (
                _validator.chat.isValidRoomUserRegistrationRequest(req.body) &&
                await _validator.user.areValidUsers(req.body.users) &&
                await _validator.chat.areUsersNewToRoom(req.params.dialogId, req.body.users)
            ) {
                let chatRoom = await ChatRoom.findOneAndUpdate(
                    { dialog_id },
                    { $pushAll: { users: req.body.users }, room_name: req.body.room_name },
                    { upsert: true, new: true }
                ).populate('users', 'email metas quickblox');

                await mentorCounselorDialogCreate(chatRoom._id);                    


                response.created(res, chatRoom);
            }
        }

        catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async updateChatRoomStatus(req, res) {
        try {
            const room_id = req.params.roomId;
            const status = req.body.status;
            if (!status || !room_id)
                throw _err.createError('BAD_REQUEST', 'Request body error');

            let room = await ChatRoom.findOneAndUpdate({ _id: room_id }, { status: status }, { new: true });
            response.modified(res, room)
        } catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    },

    async emailNotification(req, res) {
        try {
            let user_id = req.params.userId;
            let email = req.body.email;

            if (!user_id || !email)
                throw _err.createError('BAD_REQUEST', 'Request param or body error');

            let sender = await User.findById(user_id);

            let data = {
                to: email,
                sender: req.body.name,
                message: req.body.message,
                name: sender.student_details.full_name
            }

            await eventEmitter.emit('CHAT_NOTIFY', new EventData('EMAIL_NOTIFICATION_REQUEST', data));
            response.ok(res, { success: true });
        } catch (err) {
            if (!err.status)
                err = _err.createError(err.name, err.message);
            response.error(res, err);
        }
    }
};

async function mentorCounselorDialogCreate(chatRoom){
    try{
        let resp = {
            quickbloxApplication: false,
            chatRoomExist: false
        };
        console.log('mentorCounselorDialogCreate')
        let quickbloxApplication = await UniversityApplication.findOne({
            $and:
                [{ 'mentor.person': {$in: chatRoom.users } }, { 'counselor.status': 'ACCEPTED' }]
        }).populate({
                path: 'mentor.person counselor.person',
                select: 'quickblox'
            });
        let admin = await User.findOne({ email: env.ADMIN_LOGIN });
        if (quickbloxApplication) {
            resp.quickbloxApplication = true;
            let chatRoomExist = await ChatRoom.findOne({
                users: {
                    $all:
                        [quickbloxApplication.counselor.person._id, quickbloxApplication.mentor.person._id]
                }
            });

            if (!chatRoomExist) {
                let data = {
                    ids: [Number(quickbloxApplication.counselor.person.quickblox.id), Number(quickbloxApplication.mentor.person.quickblox.id)],
                    name: `${quickbloxApplication.counselor.person.quickblox.id}-${quickbloxApplication.mentor.person.quickblox.id}`
                }
                let groupDialog = await _chat.createQuickbloxDialog(data, admin.quickblox);
                await ChatRoom.create(
                    {
                        dialog_id: groupDialog._id,
                        group_id: groupDialog.xmpp_room_jid,
                        users: [admin._id, quickbloxApplication.counselor.person._id, quickbloxApplication.mentor.person._id],
                        room_name: `mentor-counselor`
                    }
                );
                resp.chatRoomCreated = true; 
            }
            console.log(resp);
            return resp;
        }
    } catch(error){
        if (!err.status)
        err = _err.createError(err.name, err.message);
        return err;
    }
}