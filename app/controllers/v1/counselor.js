'use strict'
const mongoose = require('mongoose');
const _ = require('lodash');

const User = mongoose.model('User');
const UniversityApplication = mongoose.model('UniversityApplication');
const response = require('../../middlewares/response');

const _err = require('../../helpers/v1/error');
const _user = require('../../helpers/v1/user');
const _validator = require('../../helpers/v1/validator/index');

module.exports = { 
    async updateDetails(req, res) {
        try {
            let metas = req.body.metas
			if (!metas)
				throw _err.createError('BAD_REQUEST', 'Meta Data is Required');

			let data = Object.assign({}, {metas: metas}, {
					student_details: {
						full_name: metas.reduce((full_name, meta) => {
							if (meta.key == 'first_name' || meta.key == 'last_name')
								full_name += meta.value ? meta.value + " " : "";
								return full_name
						}, "")
					}
				})
			let user = await _user.updateDetails(req.params.id, data);
            if (_validator.user.isValidUser(user))
                response.modified(res, user);
        }
        catch (err) {
            response.error(res, err);
        }
    },

    async changePassword(req, res) {
        try {
            console.log('counselor');
            let user = await _user.changePassword(null, req.params.id, req.body);
            response.modified(res, user);
        }
        catch (err) {
            console.log('Erroorrrrrr=========?????', err);
            response.error(res, err);
        }
    },

    async resetPassword(req, res) {
        try {
            console.log('counselor');
            let user = await _user.forgetPassword(null, req.params.id, req.body)
            response.modified(res, user);
        }
        catch (err) {
            console.log('Error====>', err);
            response.error(res, err);
        }
    },
    async userDetails(req, res) {
        try {
            let user = await _user.userDetails(req.params.id);
            response.ok(res, user);
        }
        catch (err) {
            console.log('Errooor====>', err);
            response.error(res, err);
        }
    },
    async updateImage(req, res) {
        try {
            let user = await _user.uploadImage(req.params.id, req.body.metas);
            if (_validator.user.isValidUser(user))
                response.modified(res, user);
        }
        catch (err) {
            console.log('Error:::::', err);
            response.error(res, err);
        }
    },

    async getApplicationRequests (req, res) {
		try {
			if (!req.params.id)
				throw _err.createError('BAD_REQUEST', 'counselor id is required');

            let applications = await UniversityApplication.find({$and: 
                [ 
                    {'counselor.person': req.params.id} , {status: 'ACTIVE'}, {'counselor.status': 'PENDING'}
                ]
            })
			.populate('user')
            .populate('university');
            
            applications = _.unionBy(applications, 'user');

			response.ok(res, applications);
		}
		catch(err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
    },
    
    async approveRejectRequest (req, res) {
        try {
            const user = await User.findById(req.params.id);
            //let application = await UniversityApplication.findById(req.params.appId);

            if (!user)
                throw _err.createError('BAD_REQUEST', 'user or application was not found');

            if (!req.body.status)
                throw _err.createError('BAD_REQUEST', 'status field is missing in request body');

			const status = req.body.status.toUpperCase().trim();
			let application = await UniversityApplication.updateMany({
								'counselor.person':user._id},
								{'counselor.status': status, $push:{chat_room:req.body.chat_room}
                            });
            if(application){
                eventEmitter.emit('APPROVE_REJECT', new EventData('SEND_APPROVE_REJECT_NOTIFY_ADMIN', {
                    to: env.MAILGUN_MAIL,//env.MAILGUN_MAIL,
                    from: user.email,
                    status: status,
                    user: application[0].user,
                    university: 'All University'
                }));
            }
            response.modified(res, { message: 'Application status updated', application }); 
        }

        catch(err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
    }
};

