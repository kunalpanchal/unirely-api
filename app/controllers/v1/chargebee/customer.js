'use strict';

const ChargebeeCustomer = require('./../../../services/chargebee/customer');

const response = require('../../../middlewares/response');

module.exports = {

    async create(req, res){
        try{
           let customer = await ChargebeeCustomer.create(req.body.user)
            response.ok(res,customer)
        }
        catch(error){
            response.error(res, error)
        }
    }
}