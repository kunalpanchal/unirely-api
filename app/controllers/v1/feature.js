'use strict';

const mongoose = require('mongoose');
const Feature = mongoose.model('Feature');
const Subscription = mongoose.model('Subscription');

const response = require('../../middlewares/response');

const _err = require('../../helpers/v1/error');
const _validator = require('../../helpers/v1/validator');

const ChargebeePlan = require('../../services/chargebee/plan');
module.exports = {

    async create(req, res){
        try{
            const feature_body = {
                _id: req.body.feature_id,
                name: req.body.name,
                description: req.body.description,
                active: req.body.active,
				metas: req.body.metas,
				parent: req.body.parent,
            }
            if(
                _validator.planFeature.isValidFeature(feature_body._id,feature_body.name)
            ){
                let feature_response = await Feature.create(feature_body);
                response.created(res, feature_response); 
            }
        }
        catch(error){
            if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
        }
	},

	async fetchSubFeature(req, res){
		let feature = await Feature.find({_id:'1_hour'}).populate('parent');
		response.ok(res,feature);
	},

	async createPlanFeature(req, res){
        try{
            const feature_body = {
				plan: req.body.plan,
                description: req.body.description,
                active: req.body.active,
				metas: req.body.metas,
				parent: req.body.parent,
            }
            if(
                _validator.planFeature.isValidFeature(feature_body._id,feature_body.name)
            ){
                let feature_response = await Feature.create(feature_body);
                response.created(res, feature_response); 
            }
        }
        catch(error){
            if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
        }
	},

	async featureAccessByUser(req,res){
		try{
            let usage = await Subscription.findOne({user:req.params.user}).select('current_usage current_limit chargebee_plan_id')
            response.ok(res, usage);
		}catch(error){
			if (!error.status)
				error = _err.createError(error.name, error.message);
			response.error(res, error);
		}
    }
	
}

