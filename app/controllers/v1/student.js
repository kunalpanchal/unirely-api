'use strict';
const _ = require('lodash');
const env = require('../../../config/environments');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const UniversityUsers = mongoose.model('UniversityUsers');
const University = mongoose.model('University');
const UniversityApplication = mongoose.model('UniversityApplication');
const Plan = mongoose.model('Plan');
const Subscription = mongoose.model('Subscription');

const ChargebeeSubscription = require('../../services/chargebee/subscription');
const ChargebeeCustomer = require('../../services/chargebee/customer');
const ChargebeeCard = require('../../services/chargebee/card');

const response = require('../../middlewares/response');

const _err = require('../../helpers/v1/error');
const _user = require('../../helpers/v1/user');
const _validator = require('../../helpers/v1/validator/index');
const _quickblox = require('../../helpers/v1/quickblox_chat');

const eventEmitter = require('../../events/event');
const EventData = require('../../interfaces/event-data');

module.exports = {
	async updateDetails(req, res) {
		try {
			let metas = req.body.metas
			if (!metas)
				throw _err.createError('BAD_REQUEST', 'Meta Data is Required');

			let data = Object.assign({}, { metas: metas }, {
				student_details: {
					full_name: metas.reduce((full_name, meta) => {
						if (meta.key == 'first_name' || meta.key == 'last_name')
							full_name += meta.value ? meta.value + " " : "";
						return full_name
					}, "")
				}
			})
			let user = await _user.studentUpdateDetails(null, req.params.id, data);
			if (_validator.user.isValidUser(user))
				response.modified(res, user);
		}
		catch (err) {
			response.error(res, err);
		}
	},

	async changePassword(req, res) {
		try {
			let user = await _user.changePassword(null, req.params.id, req.body);
			response.modified(res, user);
		}
		catch (err) {
			console.log('Erroorrrrrr=========?????', err);
			response.error(res, err);
		}
	},

	async resetPassword(req, res) {
		try {
			let user = await _user.forgetPassword(null, req.params.id, req.body)
			response.modified(res, user);
		}
		catch (err) {
			console.log('Error====>', err);
			response.error(res, err);
		}
	},
	async universityList(req, res) {
		try {
			if (req.body.dataTableAttr && req.body.dataTableAttr !== null) {
				let page_skip = req.body.dataTableAttr.page * req.body.dataTableAttr.limit;
				let limit = req.body.dataTableAttr.limit;
				let searchKey = req.body.dataTableAttr.searchKey;
				let query = {};
				if (searchKey != '') {
					let regexQ = {$regex: searchKey, $options: "i"};
					query = {
						name: regexQ
					}
				}

				let universities = await University.find(query).skip(page_skip).limit(limit);
				let totalCount = await University.count(query);
				return response.ok(res, { universities, totalCount });
			}

			let search_university = req.params.university;

			console.log(search_university,'>>>>>>>>>>>>>>>>>>>>>>>>>')
			let universityList = await _user.universityDetails(search_university);
			response.ok(res, universityList);
		}
		catch (err) {
			console.log('Error======>', err);
			response.error(res, err);
		}
	},

	async userDetails(req, res) {
		try {
			let user = await _user.userDetails(req.params.id);
			response.ok(res, user);
		}
		catch (err) {
			console.log('Errooor====>', err);
			response.error(res, err);
		}
	},
	async updateImage(req, res) {
		try {
			let user = await _user.uploadImage(req.params.id, req.body.metas);
			if (_validator.user.isValidUser(user))
				response.modified(res, user);
		}
		catch (err) {
			console.log('Error:::::', err);
			response.error(res, err);
		}
	},

	async createUniApplication(req, res) {
		try {
			const admin = await User.findOne({ email: 'management@unirely.com' });
			if (
				_validator.uniApplication.isValidUniversityApplication(req.body) &&
				_validator.user.isValidUser(admin)
			) {
				let applicationUniversities = req.body.universities
				const existingApplications = await UniversityApplication
					.find({ user: req.body.user, university: { $in: applicationUniversities }, status: 'ACTIVE' })
					.populate('university')

				if (existingApplications.length) {
					const appliedUniversities = existingApplications.map(application => application.university.id);
					applicationUniversities = applicationUniversities.filter(id => appliedUniversities.indexOf(id) === -1);
				} 

				if (applicationUniversities.length) {
					let applicationData = [];
					if (req.body.plan == 'basic') {
						let universityDetails = await University.find({ _id: { $in: applicationUniversities } }).select('addon');
						for (let university of universityDetails) applicationData.push({
							 user: req.body.user, university: university._id, admin: admin._id, addon: university.addon
						});
					}
					else applicationData = applicationUniversities
						.map(university => Object.assign({}, { 
							user: req.body.user, university, admin: admin._id
						}));


					const result = await UniversityApplication.insertMany(applicationData);
					return response.created(res, result);
				}
				throw _err.createError('DUPLICATE_RESOURCE', 'All the universities have already been applied');
			}
		}

		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async getUniversityApplications(req, res) {
		try {
			const user = await User.findById(req.params.id);
			if (_validator.user.isValidUser(user)) {
				const applications = await UniversityApplication.find({ user: user._id, status: 'ACTIVE' })
					.populate({ path: 'user', select: 'email metas student_details' })
					.populate({ path: 'university', select: 'name image' })
					.populate({ path: 'mentor.person', select: 'email metas quickblox student_details' })
					.populate({ path: 'counselor.person', select: 'email metas quickblox student_details' })
					.populate({ path: 'admin', select: 'email metas quickblox' }).lean();

				let universityIds = applications.map(application => application.university && application.university._id);

				universityIds = Array.from(new Set(universityIds));

				let uniMentors = await UniversityUsers
					.find({ university: { $in: universityIds }, type: 'MENTOR' })
					.populate({ path: 'user', select: 'email metas verified', match: { verified: { $ne: 'REJECTED' } } })
					.populate('university', '_id')
					.lean();

				uniMentors = _.groupBy(uniMentors, 'university._id');

				let responseData = {};
				responseData.connections = [];
				applications.forEach(application => {
					const university = Object.assign({ mentors: uniMentors[application.university._id] || [] }, application.university);

					responseData.user = application.user;
					responseData.counselor =  responseData.counselor || application.counselor;
					responseData.admin = application.admin;
					responseData.connections.push({ university: university, mentor: application.mentor, application_id: application._id });
				});
				response.ok(res, responseData);
			}
		}
		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			response.error(res, err);
		}
	},

	async updateProfile(req, res) {
		try {
			const user = await User.findById(req.params.id);
			const admin = await User.findOne({ email: env.ADMIN_LOGIN }).select('quickblox');
			if (_validator.user.isValidUser(user)) {
				const updateData = Object.assign({}, req.body);
				for (let key in updateData) {
					user[key] = updateData[key];
				}
				user.student_details.university_generate_form = true;
				await user.save();
				await _quickblox.adminMessage(user.quickblox, admin.quickblox, 'university_generate_list');

				// eventEmitter.emit('GENERATE_LIST', new EventData('SEND_NOTIFICATION_TO_ADMIN', {
				// 	to: env.MAILGUN_MAIL,
				// 	from: user.email,
				// }));
				return response.modified(res, {
					message: 'Updated successfully'
				});
			}
		}
		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			return response.error(res, err);
		}
	},

	async getProfile(req, res) {
		try {
			const user = await User.findById(req.params.id).lean();
			if (_validator.user.isValidUser(user)) {
				const data = Object.assign({
					email: user.email
				}, { student_details: user.student_details });
				return response.ok(res, data);
			}
		}
		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			return response.error(res, err);
		}
	},

	async subscription(req, res) {
		try {
			const userId = req.params.id;
			const plan_data = {
				plan_id: req.body.plan,
				universities: req.body.university,
				plan: await Plan.findById(req.body.plan),
				mode: req.body.mode
			};

			console.log('PLAN DATA',plan_data);

			if (!plan_data.plan || !req.params.id)
				throw _err.createError('BAD REQUEST', 'Request param or body not found');
			
				let subscription;
				let user_subscription = await Subscription.findOne({ user: req.params.id });
				if(plan_data.mode == 'card'){
					let card_detail = await ChargebeeCard.create(user_subscription.chargebee_customer_id, req.body.card);
					let customer_update = await ChargebeeCustomer.update(user_subscription.chargebee_customer_id, {auto_collection: "on"});
				}
				else {
					let customer_update = await ChargebeeCustomer.update(user_subscription.chargebee_customer_id, {auto_collection: "off"});
				}
				user_subscription.current_usage.university = Number(plan_data.universities.length) + Number(user_subscription.current_usage.university);

				if(plan_data.plan._id == 'basic'){
					let universityDetails = await University.find({ _id: { $in: plan_data.universities } }).select('price');
					let totalPrice = universityDetails.reduce( (price, university) => { return price += university.price }, 0);
					subscription = await ChargebeeSubscription.extraCharge(user_subscription.chargebee_subscription_id, totalPrice, plan_data.plan._id);
				}else {
					subscription = await ChargebeeSubscription.extraCharge(user_subscription.chargebee_subscription_id, plan_data.plan.price, plan_data.plan._id);  
				}
				user_subscription.invoice.push({
					mode: plan_data.mode.toUpperCase(),
					id: subscription.invoice ? subscription.invoice.id : null,
					university_application: plan_data.universities,
					plan: plan_data.plan._id
				})
				await user_subscription.save();
				return response.ok(res, user_subscription);
		}
		catch (err) {
			if (!err.status)
				err = _err.createError(err.name, err.message);
			return response.error(res, err);
		}
	}

};