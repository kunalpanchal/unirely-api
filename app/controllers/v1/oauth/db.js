"use strict";

const response = require('../../../middlewares/response');
const seeders = require('../../../../database/seeds/index');

const mongoose = require('mongoose');
const User = mongoose.model('User');

const EventData = require('../../../interfaces/event-data');
const eventEmitter  = require('../../../events/event');

module.exports = {

    async resetDB (req, res) {
        try {
            const db = mongoose.connection;
            let droppedCollections = [];

            for (let collectionName in db.collections) {
                droppedCollections.push(collectionName);
                /*if (collectionName === 'users') {
                    let users = await User.find({}).select('quickblox').lean();
                    eventEmitter.emit('DB_RESET', new EventData('REMOVE_QUICKBLOX_USERS', users));
                }*/
                db.collections[collectionName].drop(function (err, reply) {
                    console.log('Dropping collection ', collectionName);
                })
            }
            response.ok(res, {droppedCollections});
        }
        catch (err) {
            response.error(res, err);
        }
    },

    async seedBasic (req, res) {
        try {
            await seeders.run(null, true);
            response.created(res, { message: "Basic Seeds completed" });
        }

        catch (err) {
            response.error(res, err);
        }
    }
};