"use strict";

const env = require('../../../../config/environments');

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Role = mongoose.model('Role');
const OauthClient = mongoose.model('OauthClient');
const UniversityUsers = mongoose.model('UniversityUsers');
const Subscription = mongoose.model('Subscription');
const ChatRoom = mongoose.model('ChatRoom');

const eventEmitter = require('../../../events/event');
const response = require('../../../middlewares/response');
const EventData = require('../../../interfaces/event-data');

const _validator = require('../../../helpers/v1/validator/index');
const _hash = require('../../../helpers/v1/hash');
const _err = require('../../../helpers/v1/error');
const _oauth = require('../../../helpers/v1/oauth');
const _user = require('../../../helpers/v1/user');
const _quickblox = require('../../../helpers/v1/quickblox_chat');

module.exports = {

    async getClientCredentials(req, res) {
        try {
            let client = await OauthClient.findOne({user_id: req.user._id});
			if (_validator.oauth.isValidOauthClient(client))
                response.ok(res, {id: client.client_id, secret: client.client_secret});
        }
        catch (error) {
            response.error(res, error);
        }
    },

	async register (req, res, next) {
		try {
			if (
				_validator.oauth.isValidOauthRequest(req.body) &&
				await _validator.user.isNewUser(req.body.username.toLowerCase()) &&
				_validator.user.isValidRegisterRequest(req.body)
			) {

				let data = Object.assign({}, req.body);
				let roles = await Role.find({ name: { $in: req.body.roles.toUpperCase().split(' ') } }).select('_id name');
				let admin = await User.findOne({email: env.ADMIN_LOGIN}).select('quickblox');

				data.roles = roles.map(role => role._id)
				data.email = req.body.username.toLowerCase();
				data.student_details = req.body.student_details || null;

				if (_validator.role.areValidRoles(data.roles)) {

                    data.verification_hash = await _hash.generateBase64Secret({
                        email: data.email,
                        iat: Date.now(),
                        expiresIn: process.env.HASH_EXPIRATION || 24*60*60
                    });

                    data.quickblox = await _oauth.createQuickBloxUser({
                        login: data.email,
                        email: data.email,
                        password: env.QB_USER_PWD,
                        full_name: data.student_details ? data.student_details.full_name: data.full_name
                    }) || null;

					console.log('*****************User*************', data.quickblox);
					let qbDialog = await _oauth.createQuickbloxDialog(data.quickblox, admin.quickblox); 
					console.log(qbDialog)
					let user = await User.create(data);

					let chatMessage = await _quickblox.adminMessage(data.quickblox, admin.quickblox, 'welcome_text');

					let chatRoom = await ChatRoom.create(
						{ 	dialog_id: qbDialog._id,
							users: [admin._id, user._id] ,
							room_name: `advisor-${req.body.roles.toLowerCase()}`
						}
					);

					if(roles.every(role=> role.name.toLowerCase() == 'student') ){
						let subscription = await _user.subscription(user);
					}
									
				    if (data.university && roles.every(role=> role.name.toLowerCase() == 'mentor') ) {
				    	// const universityData = data.universities
						//     .reduce((result, uniId) => {
				    	// 	    result.push(
						// 	        ...roles.map(role => ({ user: user._id, university: uniId, type: role.name }))
						//         );
				    	// 	    return result;
						//     }, []);
						const universityData = {
							user: user._id,
							university: data.university,
							type: roles.find(role=> role.name.toLowerCase() == 'mentor').name
						}
						await UniversityUsers.create(universityData);
				    }

                    eventEmitter.emit('SIGNEDUP', new EventData('SEND_ACCOUNT_VERIFICATION_MAIL', {
                        to: user.email,
                        from: req.user.user_id.email,
                        hash: user.verification_hash
					}));
					
					next(null, user); 
				} 
			}
		}
		catch(err) {
			console.log(err)
		    response.error(res, err.status ? err : _err.getError(err.name));
		}
	},

	async resetPasswordFlowOne (req, res) {

		try {
			let email = req.params.email;

			if(_validator.user.isValidResetFlowOneRequest({ email })) {
				let user = await User.findOne({ email });

				if (_validator.user.isValidUser(user)) {
                    user.verification_hash = await _hash.generateBase64Secret({
						email: user.email,
                        iat: Date.now(),
                        expiresIn: process.env.HASH_EXPIRATION || 24*60*60
					});
					user = await user.save();

                    eventEmitter.emit('PASSWORD_RESET_REQUEST', new EventData('SEND_PASSWORD_RESET_MAIL', {
                        to: user.email,
                        from: req.user.user_id.email,
						hash: user.verification_hash,
						receiver: user.student_details.full_name
                    }));

                    response.ok(res, {
                        email: user.email,
                        verification_hash: user.verification_hash,
                        message: "A verification email has been sent to registered email."
                    });
				}
			}
		}
		catch (err) {
		    if (!err.status) {
		        err = _err.createError(new Error, err.message, 'E_INTERNAL_SERVER_ERROR', 500);
            }
			response.error(res, err);
		}
	},

	async resetPasswordFlowTwo (req, res) {
		try {
			let email = req.params.email,
                hash = req.params.hash;

			if (_validator.user.areValidResetFlowTwoParams(email, hash)) {

				let user = await User.findOne({ email });

				if (
				    _validator.user.isValidUser(user) &&
                    _validator.user.isValidVerificationHash(user, hash) &&
                    _validator.hash.hashHasNotExpired(hash)
                ) {
                    response.ok(res, {
                        message: "Email and hash verified"
                    });
				}
			}
		}
		catch (err) {
			console.log(err);
			response.error(res, err);
		}
	},

	async resetPasswordFlowThree(req, res) {
	   try {
	       let email = req.body.email;
	       let password = req.body.password;
	       let user = await User.findOne({ email });

	       if (_validator.user.isValidUser(user)) {
	           user.password = password;
	           user.verification_hash = null;
	           await user.save();

	           response.modified(res, {
	               message: "Password reset successfully"
               });
           }
       }
       catch(err) {
	       response.error(res, err);
	   }
    },

    async verifyAccount (req, res) {

        try {
            let email = req.params.email,
                hash = req.params.hash;

            if (_validator.user.isValidAccountVerificationRequest(email, hash)) {

                let user = await User.findOne({ email });

                if (
                    _validator.user.isValidUser(user) &&
                    _validator.user.isValidVerificationHash(user, hash) &&
                    _validator.hash.hashHasNotExpired(hash)
                ) {

                    user.verified = 'APPROVED';
                    user.verification_hash = null;
                    await user.save();
                    response.modified(res, {message: "User successfully verified"});
                }
            }
        }
        catch(err) {
            response.error(res, err);
        }
    }
};