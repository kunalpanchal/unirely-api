"use strict";

const _err = require('./../helpers/v1/error');

module.exports = class BaseService {
    constructor(config) {
        this.initialize(config);
    }

    initialize () {
        _err.createError (
            'METHOD_NOT_OVERRIDDEN',
            'Initialize method needs to be overridden in chat service'
        );
    }
};