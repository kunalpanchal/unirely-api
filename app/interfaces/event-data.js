"use strict";

const _err = require('./../helpers/v1/error');

class EvenData {
    constructor (action, data) {
        if (!action || !data || typeof data !== 'object')
            throw _err.createError('MALFORMED_DATA', 'Event data is malformed' );

        this.action = action;
        this.data = data;
    }
}

module.exports = EvenData;