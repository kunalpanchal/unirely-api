"use strict";

const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');

const preload = require('./preload');
const ACTIVE_APIS = require('./api');

const { requestLogger } = require('../logger');

const self = module.exports = {

    preloadAPIFiles() {
        ACTIVE_APIS.forEach(version => preload(version));
        return self;
    },
    
    setupApp (app) {
        app.disable('x-powered-by');
        app.set('env', process.env.ENV);
        app.use(requestLogger);
        app.use(cors());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));
        return self;
    },

    setupRouters(app, routers) {
        routers.forEach(router => app.use(router.mountPoint, router.router));
        return self;
    },

    setupPassport(app, passport) {
        app.use(passport.initialize());
        return self;
    }
};