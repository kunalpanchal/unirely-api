"use strict";

const fs = require('fs');
const _err = require('../../app/helpers/v1/error');
const env = require('../environments');

const ACTIVE_APIS = env.ACTIVE_APIS.split(',');

ACTIVE_APIS.forEach(version => {
    if(!fs.existsSync(`${env.ROOT_DIR}/routes/${version}`))
        throw _err.createError(
            'API_ROUTER_NOT_FOUND',
            `Specified API ${version} does not have any routes configuration listed under routes/${version}`
        );
});

module.exports = ACTIVE_APIS;