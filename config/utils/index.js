"use strict";

const preload = require('../express/preload');

module.exports = {
    cmdparser: require('./cmdparser'),

    path: require('./path'),

	preloadAPIFiles (versions) {
    	versions.forEach(version => preload(version));
	}
};