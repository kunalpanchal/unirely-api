"use strict";

const _err = require('../../../app/helpers/v1/error');

const templates = {
	'account-verification': require('./account-verification.template'),
	'reset-password': require('./reset-password.template'),
	'chat-notification': require('./chat-notification.template'),
	'approve-reject-notification-admin': require('./approve-reject-notification-admin'),
	'singup-notification-admin': require('./signup-notification-admin'),
	'generate-list-notification-admin': require('./generate-list-notification-admin'),
};

exports.get = function (name, data) {
	if (!templates[name])
		throw _err.createError('EMAIL_TEMPLATE_UNDEFINED', `Template named ${name} is not defined under config/mailer/templates`);
	return templates[name](data);
};