"use strict";

const env = require('../../environments');

module.exports = function (data) {
	return `<table style="max-width:650px;margin:0 auto;background-color:#582854;width:100%;">
				        <tr>
				            <td>
				                <table style="width:100%;height:77px;float:left;background-color:#582854;">
				                    <tr>
				                        <td style="text-align:center;">
				                            <a href="${env.APP_URL}"><img src="https://s3.amazonaws.com/unirely/Logo.png"></a>
				                        </td>
				                    </tr>
				                </table>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                <table style="width:100%;height:300px;float:left;background-color:#fff">
				                    <tr>
				                        <td style="text-align:center;padding-bottom:5%;">
											<p style="width:100%;float:left;margin:0;padding:0;font-size:18px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">
												Welcome to UniRely</p>
											<p style="width:100%;float:left;margin:0;padding-top:8px;font-size:18px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">
											It's good to have you onboard</p>
				                            <p style="width:100%;float:left;margin:0;padding-top:35px;font-size:14px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666666;">
				                                Please follow the link below to verify your account and enjoy uninterrupted services.
				                            </p>
				                            <p style="width:100%;float:left;margin:0;padding-top:32px;font-size:16px;font-family: 'Open Sans', sans-serif;color:#666666;">
				                                <a href="${data.link}" style="font-size: 16px;color: #f2f2f2;text-decoration: none;background: #500050;padding: 8px 15px;box-shadow: 0 2px 4px rgba(0,0,0,0.4);border-radius: 2px;">
			                                        VERIFY ACCOUNT
				                                </a>
				                            </p>
				                        </td>
				                    </tr>
				                </table>
				            </td>
				        </tr>
				    </table>`;
};