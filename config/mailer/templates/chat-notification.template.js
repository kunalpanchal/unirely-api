"use strict";

const env = require('../../environments');

module.exports = function (data) {
	return `<table style="max-width:650px;margin:0 auto;background-color:#582854;width:100%;">
	<tr>
		<td>
			<table style="width:100%;height:77px;float:left;background-color:#582854;">
				<tr>
					<td style="text-align:center;">
					<a href="${env.APP_URL}"><img src="https://s3.amazonaws.com/unirely/Logo.png"></a>
					</td>
				</tr>
			</table>
		</td>


	</tr>
	<tr>
		<td>
			<table style="width:100%;height:auto;float:left;background-color:#fff">
				<tr>
					<td>
						<p style="width:100%;float:left;margin:0;padding-left:15px;font-size:18px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">
							Dear ${data.receiver},</p>

						<p style="width:100%;float:left;margin:0;">
							<span style="margin:0;padding:8px 10px 10px 200px;font-size:16px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">You have received the following message from:</span>
						</p>

						<p style="width:100%;float:left;margin:0;">
							<span style="margin:0;padding:8px 10px 10px 200px;font-size:16px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">
							Name: ${data.user}
							</span>
						</p>

						<p style="width:100%;float:left;margin:0;">
							<span style="margin:0;padding:8px 10px 10px 200px;font-size:16px;line-height:28px;font-family: 'Open Sans', sans-serif;color:#666;">
							Message: ${data.message}
							</span>
						</p>
						<p style="float:left;text-align:center;width: 100%;margin:40px 0;">
							<a href="${data.link}" style="font-size: 16px;text-decoration: none;color: #fff;font-family: 'Open Sans', sans-serif;line-height: 20px;border: none;box-sizing: border-box;background-color: #582854;padding: 10px 20px;border-radius: 2px;">
							Login
							</a>
						</p>

					</td>
				</tr>

			</table>
		</td>


	</tr>



</table>


`;
};


