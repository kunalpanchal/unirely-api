"use strict";

const anyBearer = require('./strategies/bearer-any');
const adminBearer = require('./strategies/bearer-admin');
const studentBearer = require('./strategies/bearer-student');
const mentorBearer = require('./strategies/bearer-mentor');
const counselorBearer = require('./strategies/bearer-counselor');
const mentorAdminBearer = require('./strategies/bearer-mentor-admin');
const mentorcounselorBearer = require('./strategies/bearer-mentor-counselor');
const studentAdminBearer = require('./strategies/bearer-student-admin');
const clientBasic = require('./strategies/client-basic');

const basic = require('./strategies/basic');

module.exports = {
    basic,
	clientBasic,
    anyBearer,
	adminBearer,
	studentBearer,
	mentorBearer,
	counselorBearer,
	mentorAdminBearer,
	mentorcounselorBearer,
	studentAdminBearer
};