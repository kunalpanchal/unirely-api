"use strict";

module.exports = {
	password: require('./password'),
	refresh: require('./refresh')
};