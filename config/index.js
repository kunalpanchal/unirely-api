"use strict";

require('./db');
const appConfig = require('./express');
appConfig.preloadAPIFiles();

const passport = require('passport');
const auth = require('./auth')

const { getConfiguredRouters } = require('./express/route-configurator');
const chargebeeConfig = require('./chargebee');

module.exports = function (app) {

    const ACTIVE_ROUTERS = getConfiguredRouters();

    appConfig
        .setupApp(app)
        .setupRouters(app, ACTIVE_ROUTERS)
        .setupPassport(app, passport);

    auth
        .initializeOauthServer(ACTIVE_ROUTERS)
        .configure({
            name: 'passport',
            driver: passport
        })

    chargebeeConfig
        .init()
};