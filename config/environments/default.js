"use strict";

const join = require('path').join;

exports.NODE_ENV = process.env.ENV;
exports.CLUSTERED = process.env.CLUSTERED;
exports.ROOT_DIR = join(__dirname, '../..');
exports.BASE_URL = process.env.BASE_URL;
exports.APP_URL = process.env.APP_URL;
exports.LOG_DIR = process.env.LOG_DIR || `${exports.ROOT_DIR}/config/logger/logs`;
exports.LOG_LEVEL = process.env.LOG_LEVEL || 'info';
exports.PORT = process.env.PORT;
exports.DB = process.env.DB;
exports.DB_OPTIONS = {
    promiseLibrary: global.Promise,
	useMongoClient: true
};

exports.SECRET = process.env.SECRET

exports.ACTIVE_APIS = process.env.ACTIVE_APIS;

exports.QB_APP_ID = process.env.QB_APP_ID;
exports.QB_AUTH_KEY = process.env.QB_AUTH_KEY;
exports.QB_AUTH_SECRET = process.env.QB_AUTH_SECRET;
exports.QB_USER_PWD = process.env.QB_USER_PWD;

exports.MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
exports.MAILGUN_DOMAIN = process.env.MAILGUN_DOMAIN;
exports.MAILGUN_MAIL = process.env.MAILGUN_MAIL;

exports.ADMIN_LOGIN = process.env.ADMIN_LOGIN