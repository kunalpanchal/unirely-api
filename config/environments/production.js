"use strict";

exports.DB = process.env.DB_PROD;
exports.DB_OPTIONS = {
    promiseLibrary: global.Promise,
	useMongoClient: true
};