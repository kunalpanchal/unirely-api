"use strict";
require('dotenv').config();
const defaults = require('./default');
const development = Object.assign({}, defaults, require('./development'));
const production = Object.assign({}, defaults, require('./production'));
const staging = Object.assign({}, defaults, require('./staging'));

module.exports = {
    development,
    staging,
    production,
} [process.env.ENV || 'development'];
