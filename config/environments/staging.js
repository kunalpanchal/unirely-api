"use strict";

exports.DB = process.env.DB_STAGING;
exports.DB_OPTIONS = {
    promiseLibrary: global.Promise,
	useMongoClient: true
};