'use strict';

const chargebee = require('chargebee');

module.exports = {

    init(){
        chargebee.configure({
            site : process.env.CHARGEBEE_SITE, 
            api_key : process.env.CHARGEBEE_KEY});
    }
};