"use strict";

const _err = require('../../app/helpers/v1/error');

const commonSeeders = [
	'role', 'user', 'oauth-client', 'university', 'plan-feature'
];

const seeders = {
	'role': require('./role'),
	'user': require('./user'),
	'university': require('./university'),
	'oauth-client': require('./oauth-client'),
	'plan-feature': require('./plan-feature')
};

const seederNotExists = (seederName) => !seeders[seederName];

module.exports = {

	async run (extras = null) {
		try {
			for (let seederName in commonSeeders){
				if (seederNotExists(commonSeeders[seederName]))
					throw _err.createError('SEEDER_NOT_DEFINED' ,`${seederName} seeder not defined`);
				await seeders[commonSeeders[seederName]].run();
			}
	
			if (extras) {
				for (let seederName in extras){
					if (seederNotExists(seederName))
						throw _err.createError('SEEDER_NOT_DEFINED' ,`${seederName} seeder not defined`);
					await seeders[seederName].run();
				}
			}
		}
		catch (err) {
			console.log('Some error while seeding', err);
		}
	}
};