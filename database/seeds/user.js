"use strict";

const env = require('../../config/environments');

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Role = mongoose.model('Role');

const _oauth = require('./../../app/helpers/v1/oauth');
const _hash = require('./../../app/helpers/v1/hash');

module.exports = {
    async run () {
        let adminUser = await User.findOne({email: 'management@unirely.com'});

        if (adminUser)
            return;

        let adminRole = await Role.findOne({name: 'ADMIN'});

        let data = {
            full_name: 'Admin',
            email: 'management@unirely.com',
            password: 'secret',
            roles: [adminRole._id],
            verified: 'APPROVED'
        };

        data.quickblox = await _oauth.createQuickBloxUser({
            login: data.email,
            email: data.email,
            password: env.QB_USER_PWD,
            full_name: data.full_name
        }) || null;

        return User.create(data);
    }
};