"use strict";

const mongoose = require('mongoose');
const User = mongoose.model('User');
const chargebee = require('chargebee');
const defaultPlan = require('../seed-data/plan');
const Plan =  mongoose.model('Plan');

module.exports = {
	async run () {
		try {
			//let plan = await chargebee.plan.list({"status[is]" : "active"}).request();
			
			// console.log(plan)
            // plan = plan.list.map(data=>({
            //         _id: data.plan.id,
            //         name: data.plan.name,
            //         description: data.plan.description,
            //         price: data.plan.price/100,
			// 		active: true,
			// 		meta_data: data.plan.meta_data
            //     })
			// )
            await Plan.insertMany(defaultPlan);
		}
		catch (err) {
			return;
		}
	}
};