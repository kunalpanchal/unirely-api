"use strict";

const USA_UNIVERSITY = [
    {
        "name": "American University",
        "image": "https://cdn.filestackcontent.com/03BiZL5rSWCsA1puKPR0",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Babson University",
        "image": "https://cdn.filestackcontent.com/9FKW8cbaQbus6wWoLhIU",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Baruch College",
        "image": "https://cdn.filestackcontent.com/g2dCYuL9TluTUHg7MGEa",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Boston University",
        "image": "https://cdn.filestackcontent.com/1EVPBVKTtOpGFW7L6Muk",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Brandeis University",
        "image": "https://cdn.filestackcontent.com/K91wwUNpTYuYsbVjcTyW",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Brown University",
        "image": "https://cdn.filestackcontent.com/yO6t6EyiTl2ywi67X7ft",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "California Institute of Technology",
        "image": "https://cdn.filestackcontent.com/thWjXHXcTOWXh0zsmZdo",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Carnegie Mellon University",
        "image": "https://cdn.filestackcontent.com/vfwWxyaGSTCsz7v8EWT0",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Columbia University",
        "image": "https://cdn.filestackcontent.com/nDtcqz9TTqSehBALqErO",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "Cornell University",
        "image": "https://cdn.filestackcontent.com/tjUNW2vQl62VTislFxxQ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "Darthmouth College",
        "image": "https://cdn.filestackcontent.com/wr4plCz8R12dJZxwxVb5",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Drexel University",
        "image": "https://cdn.filestackcontent.com/CCfL8b2uSzmbYILqKx4z",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Duke University",
        "image": "https://cdn.filestackcontent.com/Jb0ITmFKQbqFnlSay5kh",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Emory University",
        "image": "https://cdn.filestackcontent.com/LE4HanZnS22TvF653jLF",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Fordham University",
        "image": "https://cdn.filestackcontent.com/UwwI6kkPQkixbl1V2a3x",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Georgetown University",
        "image": "https://cdn.filestackcontent.com/AnJD0PlDRweR7qcouW82",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Georgia Institute of Technology",
        "image": "https://cdn.filestackcontent.com/pKNbPUo7TsWD8VFILzkS",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Harvard University",
        "image": "https://cdn.filestackcontent.com/dOTnIUf8Q3qXG5oKxz59",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "Indiana University",
        "image": "https://cdn.filestackcontent.com/Dfr95z3PSzqHdB8dnQXg",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Johns Hopkins University",
        "image": "https://cdn.filestackcontent.com/lkBYaIp2TXO6dzgFi7iq",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Massachusetts Institute of Technology",
        "image": "https://cdn.filestackcontent.com/WsHlT4u8TeCiirl9pZRC",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "New York University",
        "image": "https://cdn.filestackcontent.com/aiVlvGX7QuiVk202VBNQ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Northeastern University",
        "image": "https://cdn.filestackcontent.com/f5dXl5PTAGXhtPsnmLh4",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Ohio State University",
        "image": "https://cdn.filestackcontent.com/tHfmd7SNQby76u96k6yR",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Pace University",
        "image": "https://cdn.filestackcontent.com/5qiKzjGkSC6SXE6MnBVX",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Pennsylvania State University--University Park",
        "image": "https://cdn.filestackcontent.com/lwhJ0FyCQfmu3yg06D10",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Princeton University",
        "image": "https://cdn.filestackcontent.com/wPAfqHZ9RfOcRGJAoB9Y",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "Purdue University",
        "image": "https://cdn.filestackcontent.com/I4Se4oZIQ3CbCsh771rQ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Rice University",
        "image": "https://cdn.filestackcontent.com/7em0pgQgQ4q7wu3tEboc",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Rutgers University--New Brunswick",
        "image": "https://cdn.filestackcontent.com/qk4T7BPYQOKUsZRnXcV5",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Stanford University",
        "image": "https://cdn.filestackcontent.com/xZr3X9S7SZGlKpRiObiS",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "Stony Brook University",
        "image": "https://cdn.filestackcontent.com/ZoEpHqQgSiahCvHMaYkY",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Syracuse University",
        "image": "https://cdn.filestackcontent.com/kqR5nhFS4atdIrwTpPr9",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Tufts University",
        "image": "https://cdn.filestackcontent.com/OUTFoKX7TYGQlrjuJB8n",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Tulane University",
        "image": "https://cdn.filestackcontent.com/QC37UjdhRGSuA3pnySkr",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California- Berkeley",
        "image": "https://cdn.filestackcontent.com/nw3mxTvJSTGUTHMjR7VU",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "University of California- Davis",
        "image": "https://cdn.filestackcontent.com/qCsg97BeS1CIOo5qt61l",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California- LA",
        "image": "https://cdn.filestackcontent.com/oveccrt3SCuv0GjByAdm",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California--Irvine",
        "image": "https://cdn.filestackcontent.com/6WctJKjERAKxjz2y8rj8",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California--San Diego",
        "image": "https://cdn.filestackcontent.com/WQCDMALQom4r4PgQqTWR",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California--Santa Barbara",
        "image": "https://cdn.filestackcontent.com/RYtZMo03TEqCiAXQWQCp",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of California--Santa Cruz",
        "image": "https://cdn.filestackcontent.com/ktDPFELNS3mEfasqdUee",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Chicago",
        "image": "https://cdn.filestackcontent.com/YtbdjtXQJmZhBkwXNUEL",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Illinois at Urbana–Champaign",
        "image": "https://cdn.filestackcontent.com/wxw6zgfFQEeB8hS1wOrY",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Iowa",
        "image": "https://cdn.filestackcontent.com/u0mb9p7ASCHRozOPdI4g",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Massachusetts--Amherst",
        "image": "https://cdn.filestackcontent.com/6zbgpCXOR5q2cDq3fxIS",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Michigan- Ann Arbor",
        "image": "https://cdn.filestackcontent.com/J7HHTmFhROar0CSGIBKd",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Minnesota--Twin Cities",
        "image": "https://cdn.filestackcontent.com/vPV0tpdzQGKEs8WWR86r",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of North Carolina- Chapel Hill",
        "image": "https://cdn.filestackcontent.com/sx7qgT8uQ5W0NacnBwTH",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Pennsylvania",
        "image": "https://cdn.filestackcontent.com/rsiZ7ITzTMGecAgpbHbN",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    },
    {
        "name": "University of Rochester",
        "image": "https://cdn.filestackcontent.com/jQvta1gRmYMLIu8WH1AP",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Southern California",
        "image": "https://cdn.filestackcontent.com/yL0WycN0QYVx1iHYSVeg",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Texas- Austin",
        "image": "https://cdn.filestackcontent.com/oSoin6ssTlWe4oa5hqg9",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Texas- Dallas",
        "image": "https://cdn.filestackcontent.com/7ukhT6cGREubiXgofem6",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Virginia",
        "image": "https://cdn.filestackcontent.com/0xvjgRSwS62JO3ksevar",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "University of Wisconsin--Madison",
        "image": "https://cdn.filestackcontent.com/DsBYfwgXSgaot1YVF6hR",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Virginia Tech",
        "image": "https://cdn.filestackcontent.com/OwflQa8NS9CJeujZ8qKI",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Wake Forest University",
        "image": "https://cdn.filestackcontent.com/JaHIuT2MSAwjG3u0UEkZ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "USA"
    },
    {
        "name": "Yale University",
        "image": "https://cdn.filestackcontent.com/qV57ZGrmTkgzFNT0M3A0",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": true,
        "country": "USA"
    }
]

const CANADA_UNIVERSITY = [
    {
        "name": "McGill University",
        "image": "https://cdn.filestackcontent.com/z0RWepPxTbGqaJFVE7JC",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    },
    {
        "name": "The University of British Columbia",
        "image": "https://cdn.filestackcontent.com/NLr8O0srRfi1Q7RFTc74",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    },
    {
        "name": "University of Alberta",
        "image": "https://cdn.filestackcontent.com/sGsoHVNRQFNYnHsmCRgQ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    },
    {
        "name": "University of Toronto",
        "image": "https://cdn.filestackcontent.com/lbqqe1EMSVeVkitmgE9j",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    },
    {
        "name": "University of Waterloo",
        "image": "https://cdn.filestackcontent.com/P9ZTknF5SpS0v8ZR0qSx",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    },
    {
        "name": "York University",
        "image": "https://cdn.filestackcontent.com/B8cXYeFJSWSuYN5omHBU",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "CANADA"
    }
]

const SINGAPORE_UNIVERSITY = [
    {
        "name": "National University of Singapore (NUS)",
        "image": "https://cdn.filestackcontent.com/O84M1w1T124zKziM5dUV",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "SINGAPORE"
    },
    {
        "name": "Nanyang Technological University",
        "image": "",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "SINGAPORE"
    },
    {
        "name": "Singapore Management University",
        "image": "https://cdn.filestackcontent.com/DVitWQjzSZmtwy8DFWIk",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "SINGAPORE"
    },
    {
        "name": "LASALLE College of the Arts",
        "image": "https://cdn.filestackcontent.com/nWZN5QryRbiOygel6dUT",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "SINGAPORE"
    }
]

const HONGKONG_UNIVERSITY = [
    {
        "name": "City University of Hong Kong",
        "image": "https://cdn.filestackcontent.com/YlnEGDxTEuIuiErJArgw",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "HONGKONG"
    },
    {
        "name": "The Hong Kong University of Science and Technology",
        "image": "https://cdn.filestackcontent.com/NIEZbW4QmuCoMlBitq78",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "HONGKONG"
    },
    {
        "name": "University of Hong Kong",
        "image": "https://cdn.filestackcontent.com/Jj8dRIvURCO3vRlEIiQe",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "HONGKONG"
    }
]

const UK_UNIVERSITY = [
    {
        "name": "Birmingham City University",
        "image": "https://cdn.filestackcontent.com/ZkgXu2EVSfCQ1kR9VXRu",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Brunel University, Uxbridge and London",
        "image": "https://cdn.filestackcontent.com/GiwXeOHTm6Tci7TbW1NI",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "City, University of London",
        "image": "https://cdn.filestackcontent.com/pXVoBaHwQLWio24vaTtM",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Durham University, Durham",
        "image": "https://cdn.filestackcontent.com/JZms8ocPS1y7rA6PM8fi",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Heriot-Watt University, Edinburgh and Galashiels",
        "image": "https://cdn.filestackcontent.com/h6qQ2jaMQuqHa4SbrK2J",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Imperial College London",
        "image": "https://cdn.filestackcontent.com/lAWGGDJrR9SRQWBbPdZA",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "King's College London",
        "image": "https://cdn.filestackcontent.com/cmD9YfnbQgmSAQ5ynsrA",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Lancaster University",
        "image": "https://cdn.filestackcontent.com/fIWNUaEORCuA78XJlNfG",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "London Business School",
        "image": "https://cdn.filestackcontent.com/tVD8zTiSYarkyzQm8HOg",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "London School of Economics and Political Science (LSE)",
        "image": "https://cdn.filestackcontent.com/pC3NlP3TqC3z2Ru5uURe",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Loughborough University",
        "image": "https://cdn.filestackcontent.com/GCef0hqUTf2HGi8fL6Yd",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Manchester Metropolitan University",
        "image": "https://cdn.filestackcontent.com/gxnmHHSdTReCZ5LORKta",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Middlesex University, London",
        "image": "https://cdn.filestackcontent.com/IggruT9SQmRD7LTNvHWC",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Newcastle University",
        "image": "https://cdn.filestackcontent.com/BCRFKFclQPSmTw70tuso",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Newman University, Birmingham",
        "image": "https://cdn.filestackcontent.com/6nzjzS7DSb6SH90tkxt6",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Oxford Brookes University",
        "image": "https://cdn.filestackcontent.com/cbthtLSlRPWfoe2HcCA2",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "Queen Mary, University of London",
        "image": "https://cdn.filestackcontent.com/x3mnlDIWQyqzuNP9USyh",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University College London (UCL)",
        "image": "https://cdn.filestackcontent.com/TqRx5mYATwWyzoSexXJe",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Bath",
        "image": "https://cdn.filestackcontent.com/6xOKJrKSu6gAag7s7LGQ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Birmingham",
        "image": "https://cdn.filestackcontent.com/Tf5ilInXRJubrlKVCBBw",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Bristol",
        "image": "https://cdn.filestackcontent.com/8305nWpDS1SrbfYdnDCt",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Cambridge",
        "image": "https://cdn.filestackcontent.com/tkLIWaMfRUaOWIw46jPJ",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of East London",
        "image": "https://cdn.filestackcontent.com/rQr3kMYXTzOB6CykKA3a",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Edinburgh",
        "image": "https://cdn.filestackcontent.com/0O9dYu1zRBedGVvwUd8R",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Exeter",
        "image": "https://cdn.filestackcontent.com/Of3LUVwSHuwMgch2EgC9",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Glasgow",
        "image": "https://cdn.filestackcontent.com/08wr2YCcSLezQijQLoXS",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Leeds",
        "image": "https://cdn.filestackcontent.com/x6BSefldTf6Ka4lm2KQX",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of London",
        "image": "https://cdn.filestackcontent.com/HQMZotgUQGGDBjYoDMPi",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Manchester",
        "image": "https://cdn.filestackcontent.com/H23z87UHSbytaEVViGIx",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Nottingham",
        "image": "https://cdn.filestackcontent.com/e0rT2AF7SaymQCyqvPrK",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Oxford",
        "image": "https://cdn.filestackcontent.com/yPmOvL4QqqMqwCwBweCw",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Southampton",
        "image": "https://cdn.filestackcontent.com/s6tsHUY5QTyAR8pzmY6L",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of St Andrews",
        "image": "https://cdn.filestackcontent.com/tROvMxCRC5Dc5CqOnvsA",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Warwick, Coventry",
        "image": "https://cdn.filestackcontent.com/0lLbu11gShyZPnttk9q5",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of Westminster, London",
        "image": "https://cdn.filestackcontent.com/i0vQNTBdRouHHsk3i9td",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    },
    {
        "name": "University of York",
        "image": "https://cdn.filestackcontent.com/4OzsMhlQ4y3miLBUTKA1",
        "description": "",
        "address": "",
        "contact": "",
        "price": 399,
        "priority": false,
        "country": "UK"
    }
];

module.exports = [
    ...USA_UNIVERSITY,
    ...CANADA_UNIVERSITY,
    ...SINGAPORE_UNIVERSITY,
    ...HONGKONG_UNIVERSITY,
    ...UK_UNIVERSITY
]