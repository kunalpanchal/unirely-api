"use strict";

require('dotenv').config();
const activeAPIs = require('../config/express/api').getActiveAPIS();
require('../config/utils').preloadAPIFiles(activeAPIs);

const parseArgs = require('minimist');
const mongoose = require('mongoose');
const seeders = require('./seeds');
const envConfig = require('../config/environments');

mongoose.Promise = global.Promise;

let additionalSeeds = parseArgs(process.argv.splice(2))['_'];

const RESET_DB = (function () {
    let indexOfReset = additionalSeeds.indexOf('reset');
    if (indexOfReset >= 0) {
        additionalSeeds.splice(indexOfReset, 1);
        return true;
    }
    return false;
})();

const _resetDB = async function () {

    return new Promise(function (resolve, reject) {
        const db = mongoose.connection;
        let droppedCollections = [];
        for (let collectionName in db.collections) {
            db.collections[collectionName].drop(function (err, reply) {
                droppedCollections.push(collectionName);
            })
        }
        setTimeout(resolve(droppedCollections), 3000);
    });
};

mongoose.set('debug', true);

mongoose.connect(envConfig.db, envConfig.dbOptions);

mongoose.connection.on('connected', async function () {
    console.info(`Connected to mongoose on ${envConfig.db}`);
    if (RESET_DB) {
        await _resetDB();
        seeders.run(additionalSeeds.length || null)
            .then(function () {
                mongoose.connection.close(function () {
                    console.log('Mongoose default connection disconnected post seeding');
                    process.exit(0);
                });
            });
    }

    else
        seeders.run(additionalSeeds.length || null)
            .then(function () {
                mongoose.connection.close(function () {
                    console.log('Mongoose default connection disconnected post seeding');
                    process.exit(0);
                });
            });
});