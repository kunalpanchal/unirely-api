"use strict";

module.exports = [
	{
		"_id": "basic",
		"name": "Basic Plan",
		"description": "Team of 2 People",
		"active": "true",
		"price": 399,
		"university_limit": -1
	},
	{
		"_id": "team-of-six_special",
		"name": "Five Uni Plan",
		"description": "Team of 6 People",
		"active": "true",
		"price": 1199,
		"university_limit": 5
	},
	{
		"_id": "team-of-eleven_special",
		"name": "Ten Uni Plan",
		"description": "Team of 11 People",
		"active": "true",
		"price": 1999,
		"university_limit": 10
	},
];