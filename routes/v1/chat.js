"use strict";

const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');
const chat = require ('../../app/controllers/v1/chat');

module.exports = [
    { method: 'POST', path: '/chat/user/:userId/get_rooms', handlers: [ isAuthenticated(), chat.getUserRooms ] },
    { method: 'POST', path: '/chat/user/:userId/rooms', handlers: [ isAuthenticated(), chat.registerUserToRooms ] },

    { method: 'GET', path: '/chat/room/:dialogId/users', handlers: [ isAuthenticated(), chat.getRoomUsers ] },
    { method: 'POST', path: '/chat/room/:dialogId/users', handlers: [ isAuthenticated(), chat.registerUsersToRoom ] },

    { method: 'PUT', path: '/chat/room/status/:roomId', handlers:[chat.updateChatRoomStatus]},

    { method: 'POST', path: '/chat/message/user/:userId/notify', handlers:[chat.emailNotification]},

    {method: 'POST', path: '/chat/room/user/:userId', handlers: [ chat.registerChatRoom ]}
];