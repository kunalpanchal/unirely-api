"use strict";

const plan = require('../../app/controllers/v1/plan');
const feature = require('../../app/controllers/v1/feature');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports = [
	/*
		Plan Routes
	*/
        {
                method: 'POST',
                path: '/plan',
                handlers: [plan.create]
        },
        {
                method: 'GET',
                path: '/plan/:plan',
                handlers: [plan.fetch]
        },
        {
                method: 'PUT',
                path: '/plan/:plan',
                handlers: [plan.update]
        },
        {
                method: 'GET',
                path: '/feature_access/plan/:plan',
                handlers: [plan.featureAccessByPlan]
        },
	/*
		Feature Routes
	*/
        {
                method: 'POST',
                path: '/feature',
                handlers: [feature.create]
        },
        {
                method: 'GET',
                path: '/feature',
                handlers: [feature.fetchSubFeature]
        },
        {
                method: 'GET',
                path: '/feature_access/user/:user',
                handlers: [feature.featureAccessByUser]
        },
]