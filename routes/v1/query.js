"use strict";

const query = require('../../app/controllers/v1/query');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports = [
    { path: "/query/user_details", method: "GET", handlers: [ isAuthenticated('admin'), query.userStudentDetails ] },
    { path: "/query/user_verification", method: "GET", handlers: [ isAuthenticated('admin'), query.userVerification ] }
];