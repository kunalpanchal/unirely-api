"use strict";

const ChargebeeCustomer = require('../../app/controllers/v1/chargebee/customer');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports =[
    {
		method: 'POST',
		path: '/subscription/customer/create',
		handlers: [ChargebeeCustomer.create]
	},
]