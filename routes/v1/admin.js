"use strict";

const admin = require('../../app/controllers/v1/admin.js');
const UniApplication = require('../../app/controllers/v1/university-application');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');
module.exports = [
    {
        method: 'POST',
        path: '/admin/students',
        handlers: [ isAuthenticated('admin'), admin.getAllStudents ]
    },

    {
        method: 'POST',
        path: '/admin/mentors',
        handlers: [ isAuthenticated('admin'), admin.getAllMentors ]
    },

	{
		method: 'PUT',
		path: '/admin/mentor/:id/:status',
		handlers: [ admin.approveMentor ]
	},

	{
		method: 'POST',
		path: '/admin/counselors',
		handlers: [ isAuthenticated('admin'), admin.getAllCounselors ] 
	},

	{
		method: 'PUT',
		path: '/admin/counselor/:id/approve',
		handlers: [ admin.approvecounselor ]
	},

	{
		method: 'GET',
		path: '/admin/university/:id/mentors',
		handlers: [ isAuthenticated('admin'), admin.getUniversityMentors ]
	},

	{
		method: 'POST',
		path: '/admin/universities/mentors',
		handlers: [ isAuthenticated('admin'), admin.getUniversitiesMentors ]
	},

	{
		method: 'PUT',
		path: '/application/:application/mentor/:mentor',
		handlers: [ isAuthenticated('admin'), admin.assignMentorToApplication ]
	},

	{
		method: 'PUT',
		path: '/applications/mentors',
		handlers: [ isAuthenticated('admin'), admin.assignMentorsToApplications ]
	},


	{
		method: 'PUT',
		path: '/student/:student/counselor/:counselor',
		handlers: [ isAuthenticated('admin'), admin.assigncounselorToApplications ]
	},

	{
		method: 'GET',
		path: '/university-applications',
		handlers: [ isAuthenticated('admin'), admin.fetchAllApplications ]
	},
	{
		method: 'PUT',
		path: '/admin/university-applications',
		handlers: [ isAuthenticated('admin'), admin.updateUniversityApplication ]
	},
	{
		method: 'POST',
		path: '/admin/university',
		handlers: [ isAuthenticated('admin'), admin.universityUpdate]
	}
];