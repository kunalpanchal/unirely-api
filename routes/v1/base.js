"use strict";

const base = require('../../app/controllers/v1/base');

module.exports = [
    { path: "", method: "GET", handlers: [ base.index ] }
];