"use strict";

const student = require('../../app/controllers/v1/student');
const UniApplication = require('../../app/controllers/v1/university-application');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports = [
    { path: '/student/:id/profile-edit', method: 'PUT', handlers: [isAuthenticated('student'), student.updateDetails] },
    { path: '/student/:id/password', method: 'PUT', handlers: [student.changePassword] },
    { path: '/student/:id/reset-password', method: 'PUT', handlers: [isAuthenticated('student'), student.resetPassword] },
    { path: '/student/:id/profile-pic', method: 'PUT', handlers: [isAuthenticated('student'), student.updateImage] },
    { path: '/student/university-list/:university?', method: 'POST', handlers: [student.universityList] },
    { path: '/student/:id/user-details', method: 'GET', handlers: [ isAuthenticated('student'), student.userDetails] },
	{
		method: 'GET',
		path: '/student/:id/university-applications',
		handlers: [isAuthenticated('student-or-admin'), student.getUniversityApplications]
	},
	{
		method: 'POST',
		path: '/universities/apply',
		handlers: [ isAuthenticated('student'), student.createUniApplication ]
	},
	{
		method: 'PUT',
		path: '/student/:id/update-profile',
		handlers: [ student.updateProfile ]
	},
	{
		method: 'GET',
		path: '/student/:id/profile',
		handlers: [ student.getProfile ]
	},
	{
		method: 'POST',
		path: '/student/:id/plan/subscription',
		handlers: [ student.subscription ]
	}
];
