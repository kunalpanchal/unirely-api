"use strict";

const routes = [
    ...require('./base'),
    ...require('./oauth'),
    ...require('./mentor'),
    ...require('./student'),
    ...require('./counselor'),
    ...require('./chat'),
    ...require('./admin'),
    ...require('./subscription'),
    ...require('./plan-feature'),
    ...require('./query'),
];

module.exports = routes;