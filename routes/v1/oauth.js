"use strict";

const oauth = require('../../app/controllers/v1/oauth');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

// Login and register routes are in config/auth/oauth/routes

module.exports = [
    {
        method: 'GET',
        path: '/oauth/db/reset',
        handlers: [ isAuthenticated('client'), oauth.db.resetDB ]
    },

    {
        method: 'GET',
        path: '/oauth/db/seed',
        handlers: [ oauth.db.seedBasic ]
    },

    {
        method: 'POST',
        path: '/oauth/client',
        handlers: [ isAuthenticated('basic'), oauth.user.getClientCredentials ]
    },

    {
        method: 'GET',
        path: '/oauth/verify/:email/:hash',
        handlers: [ isAuthenticated('client'), oauth.user.verifyAccount ]
    },

    {
        method: 'GET',
        path: '/oauth/reset/password/:email',
        handlers: [ isAuthenticated('client'), oauth.user.resetPasswordFlowOne ]
    },

    {
        method: 'GET',
        path: '/oauth/reset/password/:email/:hash',
        handlers: [ isAuthenticated('client'), oauth.user.resetPasswordFlowTwo ]
    },

    {
        method: 'PUT',
        path: '/oauth/reset/password',
        handlers: [ isAuthenticated('client'), oauth.user.resetPasswordFlowThree ]
    },
];