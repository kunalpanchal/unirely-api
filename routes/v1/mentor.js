'use strict';

const mentor = require('../../app/controllers/v1/mentor');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports = [
    { path: '/mentor/:id/profile-edit', method: 'PUT', handlers: [isAuthenticated('mentor-or-admin'), mentor.updateDetails] },
    { path: '/mentor/:id/password', method: 'PUT', handlers: [isAuthenticated('mentor'), mentor.changePassword] },
    { path: '/mentor/:id/reset-password', method: 'PUT', handlers: [isAuthenticated('mentor'), mentor.resetPassword] },
    { path: '/mentor/:id/profile-pic', method: 'PUT', handlers: [isAuthenticated('mentor'), mentor.updateImage] },
    { path: '/mentor/:id/user-details', method: 'GET', handlers: [isAuthenticated('mentor'), mentor.userDetails] },
	{
		path: '/mentor/:id/universities',
		method: 'PUT',
		handlers: [ isAuthenticated('mentor-or-admin'), mentor.editUniversities ]
	},
	{
		method: 'GET',
		path: '/mentor/:id/university-applications',
		handlers: [ mentor.getApplicationRequests ]
    },
    {
		method: 'PUT',
		path: '/mentor/:id/application/:appId',
		handlers: [ mentor.approveRejectRequest ]
	}
]