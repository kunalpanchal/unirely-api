'use strict';

const counselor = require('../../app/controllers/v1/counselor');
const isAuthenticated = require('../../app/middlewares/v1/isAuthenticated');

module.exports = [
    { path: '/counselor/:id/profile-edit', method: 'PUT', handlers: [counselor.updateDetails] },
    { path: '/counselor/:id/password', method: 'PUT', handlers: [isAuthenticated('counselor'),counselor.changePassword] },
    { path: '/counselor/:id/reset-password', method: 'PUT', handlers: [isAuthenticated('counselor'),counselor.resetPassword] },
    { path: '/counselor/:id/profile-pic', method: 'PUT', handlers: [isAuthenticated('counselor'), counselor.updateImage] },
    { path: '/counselor/:id/user-details', method: 'GET', handlers: [isAuthenticated('counselor'), counselor.userDetails] },
    {
		method: 'GET',
		path: '/counselor/:id/university-applications',
		handlers: [ counselor.getApplicationRequests ]
    },
    {
		method: 'PUT',
		path: '/counselor/:id/application/',
		handlers: [ counselor.approveRejectRequest ]
	}
]