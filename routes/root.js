"use strict";

const root = require('../app/controllers/root');

const API_ROUTES = [
	{ path: '', method: 'GET', handlers: [ root.index ] },
    { path: '*', method: 'ALL', handlers: [ root.error ] }
];

module.exports = API_ROUTES;